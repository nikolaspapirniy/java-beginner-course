package com.thinkascoder.system.validations;

import com.thinkascoder.system.HomeworkNotCorrectException;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class MethodValidations extends ReflectionHelper {
    public static void assertHasMethod(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (Modifier.isStatic(method.getModifiers())) {
            String errorMessage = "В классе " + className.getSimpleName() + " метод " + methodName + " не должен быть статическим";
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertHasNoMethod(Class className, String methodName) {
        Method method = getMethodFromMethodList(methodName, className.getDeclaredMethods());
        if (method != null) {
            String errorMessage = "В классе " + className.getSimpleName() + " не должно быть метода " + methodName;
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertHasStaticMethod(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (!Modifier.isStatic(method.getModifiers())) {
            String errorMessage = "В классе " + className.getSimpleName() + " метод " + methodName + " должен быть статическим";
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    private static Method getAndAssertMethod(Class className, String methodName) {
        Method method = getMethodFromMethodList(methodName, className.getDeclaredMethods());
        if (method == null) {
            String errorMessage = "В классе " + className.getSimpleName() + " отсутствует метод " + methodName;
            throw new HomeworkNotCorrectException(errorMessage);
        }
        return method;
    }

    public static void assertMethodHasNoParams(Class className, String methodName) {
        assertMethodParams(className, methodName);
    }

    public static void assertMethodParams(Class className, String methodName, Class... params) {
        try {
            Method method = className.getDeclaredMethod(methodName, params);
        } catch (NoSuchMethodException e) {
            StringBuilder errorMessage = new StringBuilder()
                    .append("В классе ")
                    .append(className.getSimpleName())
                    .append(" у метода метод ")
                    .append(methodName)
                    .append(" не верный наборы параметор");

            throw new HomeworkNotCorrectException(errorMessage.toString());
        }
    }

    public static void assertMethodReturn(Class className, String methodName, Class returnType) {
        Method method = getMethodFromMethodList(methodName, className.getDeclaredMethods());
        if (!method.getReturnType().equals(returnType)) {
            String errorMessage = "В классе " + className.getSimpleName() + " у метода " + methodName + " не верный возвращаемый тип";
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertMethodReturnsNothing(Class className, String methodName) {
        assertMethodReturn(className, methodName, void.class);
    }

    public static void assertHasInheritedMethod(Class className, String methodName) {
        Method method = getMethodFromMethodList(methodName, className.getMethods());
        if (method == null) {
            String errorMessage = "В классе " + className.getSimpleName() + " нет наследуемого метода " + methodName;
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertMethodIsPublic(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (!Modifier.isPublic(method.getModifiers())) {
            throw new HomeworkNotCorrectException("В классе " + className.getSimpleName() + " метод " + methodName + " должен быть публичным");
        }
    }

    public static void assertMethodIsPrivate(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (!Modifier.isPrivate(method.getModifiers())) {
            throw new HomeworkNotCorrectException("В классе " + className.getSimpleName() + " метод " + methodName + " должен быть приватным");
        }
    }

    public static void assertMethodIsProtected(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (!Modifier.isProtected(method.getModifiers())) {
            throw new HomeworkNotCorrectException("В классе " + className.getSimpleName() + " метод " + methodName + " должен быть protected");
        }
    }

    public static void assertMethodIsPublicOrProtected(Class className, String methodName) {
        Method method = getAndAssertMethod(className, methodName);
        if (!Modifier.isPublic(method.getModifiers()) && !Modifier.isProtected(method.getModifiers())) {
            throw new HomeworkNotCorrectException("В классе " + className.getSimpleName() + " метод " + methodName + " должен быть public или protected");
        }
    }
}
