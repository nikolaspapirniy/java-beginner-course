package com.thinkascoder.system.validations;

import com.thinkascoder.system.HomeworkNotCorrectException;

import java.lang.reflect.Constructor;

public class ConstructorValidations extends ReflectionHelper {
    public static void assertHasDefaultConstructor(Class className) {
        if (!hasDefaultConstructor(className)) {
            String errorMessage = "В классе " + className.getSimpleName() + " должен быть публичный конструктор без параметров";
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertHasNoDefaultConstructor(Class className) {
        if (hasDefaultConstructor(className)) {
            String errorMessage = "В классе " + className.getSimpleName() + " не должно быть публичного конструктора без параметров";
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    private static boolean hasDefaultConstructor(Class className) {
        Constructor[] constructors = className.getDeclaredConstructors();
        boolean hasDefaultConstructor = false;
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getParameterCount());
            if (constructor.getParameterCount() == 0)
                hasDefaultConstructor = true;
        }
        return hasDefaultConstructor;
    }

    public static void assertHasConstructor(Class className, Class... params) {
        try {
            boolean foundConstructor = false;
            for (Constructor declaredConstructor : className.getDeclaredConstructors()) {
                Class[] parameterTypes = declaredConstructor.getParameterTypes();
                if (parameterTypes.length == params.length) {
                    if (checkConstructorParams(parameterTypes, params))
                        foundConstructor = true;
                }
            }

            if (!foundConstructor)
                throw new HomeworkNotCorrectException("no constructor");
        } catch (Exception e) {
            StringBuilder errorMessage = new StringBuilder()
                    .append("В классе ")
                    .append(className.getSimpleName())
                    .append(" отсутствует конструктор ")
                    .append(" с параметрами [");
            for (int i = 0; i < params.length; i++) {
                errorMessage.append(params[i].getSimpleName());
                if (i != params.length - 1)
                    errorMessage.append(", ");
            }
            errorMessage.append("]");

            throw new HomeworkNotCorrectException(errorMessage.toString());
        }
    }

    private static boolean checkConstructorParams(Class[] parameterTypes, Class[] params) {
        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i] != params[i])
                return false;
        }
        return true;
    }
}
