package com.thinkascoder.system.validations;

import com.thinkascoder.system.HomeworkNotCorrectException;
import com.thinkascoder.system.HustonWeHaveAProblemException;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ClassValidations extends ReflectionHelper {
    public static final String COM_THINKASCODER_PACKAGE = "com.thinkascoder";
    private static Reflections reflections;

    static {
        List<ClassLoader> classLoadersList = new LinkedList<>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("com.thinkascoder"))));
    }

    public static Class assertClass(String className) {
        Class classByName = getClassByName(className);
        if (classByName == null)
            throw new HomeworkNotCorrectException("Класс " + className + " не найден");

        return classByName;
    }

    private static Class getClassByName(String className) {
        Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
        for (Class<?> aClass : classes) {
            if (aClass.getSimpleName().equals(className))
                return aClass;
        }
        return null;
    }

    public static void assertInheritance(Class parent, String child) {
        Class aClass = assertClass(child);
        assertInheritance(parent, aClass);
    }

    public static void assertInheritance(Class parent, Class child) {
        if(child == null)
            throw new HustonWeHaveAProblemException();

        Reflections reflections = new Reflections(COM_THINKASCODER_PACKAGE);
        Set<Class> classes = reflections.getSubTypesOf(parent);
        boolean isInherited = false;
        for (Class aClass : classes) {
            if (aClass.equals(child))
                isInherited = true;

        }

        if (!isInherited) {
            String errorMessage = "Класс " + child.getSimpleName() + " не наследуется от класса " + parent.getSimpleName();
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    public static void assertClassImplementsInterface(Class parent, Class childInterface) {
        if(childInterface == null)
            throw new HustonWeHaveAProblemException();

        Class[] interfaces = parent.getInterfaces();
        boolean isImplemented = false;
        for (Class i : interfaces) {
            if (i.equals(childInterface))
                isImplemented = true;
        }

        if (!isImplemented) {
            String errorMessage = "Класс " + parent.getSimpleName() + " не имплементирует интерфейс " + childInterface.getSimpleName();
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }
}
