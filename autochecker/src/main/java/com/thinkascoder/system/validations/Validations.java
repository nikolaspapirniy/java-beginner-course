package com.thinkascoder.system.validations;

import com.thinkascoder.system.HandsOnNotCorrectException;
import org.junit.Assert;

import java.util.Arrays;

import static org.junit.Assert.fail;

public class Validations {
    protected static final Object ___ = "Nothing";

    // Simple types
    protected static Class<?> getType(short value) {
        return short.class;
    }

    protected static Class<?> getType(String value) {
        return String.class;
    }

    protected static Class<?> getType(double value) {
        return double.class;
    }

    protected static Class<?> getType(float value) {
        return float.class;
    }

    protected static Class<?> getType(byte value) {
        return byte.class;
    }

    protected static Class<?> getType(int value) {
        return int.class;
    }

    protected static Class<?> getType(char value) {
        return char.class;
    }

    protected static Class<?> getType(long value) {
        return long.class;
    }

    // Arrays
    protected static Class<?> getType(int[] array) {
        return int[].class;
    }

    protected static Class<?> getType(short[] array) {
        return short[].class;
    }

    protected static Class<?> getType(double[] array) {
        return double[].class;
    }


    protected static Class<?> getType(long[] array) {
        return long[].class;
    }

    protected static Class<?> getType(byte[] array) {
        return byte[].class;
    }


    protected static Class<?> getType(boolean[] array) {
        return boolean[].class;
    }

    protected static Class<?> getType(float[] array) {
        return float[].class;
    }

    protected static Class<?> getType(char[] array) {
        return char[].class;
    }

    protected static Class<?> getType(String[] array) {
        return String[].class;
    }

    // Wrappers
    protected static Class<?> getWrapperType(short value) {
        return Short.class;
    }

    protected static Class<?> getWrapperType(double value) {
        return Double.class;
    }

    protected static Class<?> getWrapperType(float value) {
        return Float.class;
    }

    protected static Class<?> getWrapperType(byte value) {
        return Byte.class;
    }

    protected static Class<?> getWrapperType(int value) {
        return Integer.class;
    }

    protected static Class<?> getWrapperType(char value) {
        return Character.class;
    }

    protected static Class<?> getWrapperType(long value) {
        return Long.class;
    }

    protected static Class<?> getWrapperType(boolean value) {
        return Boolean.class;
    }


    protected static void assertEquals(Object a, Object b) {
        if (checkIsNotNull(a, b))
            return;

        checkIsSolved(b);
        assertThatEquals(a, b);
    }

    // Asserts

    protected static void assertEquals(char a, char b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(int a, int b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(short a, short b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(long a, long b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(byte a, byte b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(float a, float b) {
        Assert.assertEquals(a, b, 0.01);
    }

    protected static void assertEquals(boolean a, boolean b) {
        assertThatEquals(a, b);
    }

    protected static void assertEquals(double a, double b) {
        Assert.assertEquals(a, b, 0.01);
    }

    // Mix asserts
    protected static void assertEquals(Integer a, int b) {
        assertThatEquals(a, b);
    }

    protected static void assertThatEquals(Object a, Object b) {
        if (checkIsNotNull(a, b))
            return;

        if (!a.equals(b))
            throw new HandsOnNotCorrectException("Решение не верное!");
    }

    private static boolean checkIsNotNull(Object a, Object b) {
        if (a == null || b == null) {
            if (a != b)
                throw new HandsOnNotCorrectException("Решение не верное!");
            else
                return true;
        }
        return false;
    }

    private static void checkIsSolved(Object b) {
        if (b instanceof String && b.equals(___))
            fail("Не забудь написать ответ вместо ___");
    }
}
