package com.thinkascoder.system.validations;

import java.lang.reflect.Method;

public abstract class ReflectionHelper {
    protected static Method getMethodFromMethodList(String methodName, Method[] methods) {
        Method[] declaredMethods = methods;
        for (Method method : declaredMethods) {
            if (method.getName().equals(methodName))
                return method;
        }
        return null;
    }
}
