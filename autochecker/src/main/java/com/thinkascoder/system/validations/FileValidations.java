package com.thinkascoder.system.validations;

import com.thinkascoder.system.HandsOnNotCorrectException;
import com.thinkascoder.system.HomeworkNotCorrectException;

import java.io.File;
import java.net.URL;

public class FileValidations {
    public static void assertFileExists(File file) {
        if(!file.exists())
            throw new HandsOnNotCorrectException("Файл " + file.getName() + " не найден");
    }
}
