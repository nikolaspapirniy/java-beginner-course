package com.thinkascoder.system.validations;

import com.thinkascoder.system.HomeworkNotCorrectException;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;
import sun.reflect.generics.reflectiveObjects.TypeVariableImpl;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Set;

import static org.reflections.ReflectionUtils.getAllFields;

public class FieldValidations extends ReflectionHelper {
    public static void assertHasField(Class className, String fieldName, Class fieldType) {
        try {
            Field field = className.getDeclaredField(fieldName);
            if (!fieldType.equals(field.getType())) {
                String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " следовало бы сделать типа " + fieldType.getSimpleName();
                throw new HomeworkNotCorrectException(errorMessage);
            }

        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    public static void assertHasNoField(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);
            if (field != null) {
                String errorMessage = "В классе " + className.getSimpleName() + " не должно быть поля " + fieldName;
                throw new HomeworkNotCorrectException(errorMessage);
            }
        } catch (NoSuchFieldException e) {
            // no field
        }
    }

    public static void assertPublicField(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);
            if (!Modifier.isPublic(field.getModifiers())) {
                String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " должно быть публичным";
                throw new HomeworkNotCorrectException(errorMessage);
            }
        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    public static void assertPrivateField(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);
            if (!Modifier.isPrivate(field.getModifiers())) {
                String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " должно быть приватным";
                throw new HomeworkNotCorrectException(errorMessage);
            }
        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    public static void assertHasInheritedField(Class className, String fieldName, Class fieldType) {
        Set<Field> fields = getAllFields(className);
        boolean hasField = false;
        for (Field field : fields) {
            boolean isPublicOrProtected = Modifier.isPublic(field.getModifiers()) || Modifier.isProtected(field.getModifiers());
            if (field.getName().equals(fieldName) && isPublicOrProtected) {
                if (!fieldType.equals(field.getType())) {
                    String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " следовало бы сделать типа " + fieldType.getSimpleName();
                    throw new HomeworkNotCorrectException(errorMessage);
                } else {
                    hasField = true;
                }
            }
        }

        if (!hasField) {
            String errorMessage = "В клас " + className.getSimpleName() + " не наследуется поле " + fieldName;
            throw new HomeworkNotCorrectException(errorMessage);
        }
    }

    private static void throwNoFieldException(Class className, String fieldName) {
        String errorMessage = "В классе " + className.getSimpleName() + " отсутствует поле " + fieldName;
        throw new HomeworkNotCorrectException(errorMessage);
    }

    public static void assertFinalField(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);
            if (!Modifier.isFinal(field.getModifiers())) {
                String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " должно быть финальным";
                throw new HomeworkNotCorrectException(errorMessage);
            }
        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    public static void assertHasGenericField(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);

            Type genericType = field.getGenericType();
            boolean isGenericType = genericType instanceof TypeVariableImpl || genericType instanceof GenericArrayType;
            if (!isGenericType) {
                String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " не generic типа";
                throw new HomeworkNotCorrectException(errorMessage);
            }
        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    public static void assertFieldTypeWithGeneric(Class className, String fieldName) {
        try {
            Field field = className.getDeclaredField(fieldName);

            Type genericType = field.getGenericType();
            if (!(genericType instanceof ParameterizedTypeImpl)) {
                throwFieldHasNoGenericPatamException(className, fieldName);
            } else {
                ParameterizedTypeImpl parameterizedType = (ParameterizedTypeImpl) genericType;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                if (actualTypeArguments.length == 0) {
                    throwFieldHasNoGenericPatamException(className, fieldName);
                }
            }
        } catch (NoSuchFieldException e) {
            throwNoFieldException(className, fieldName);
        }
    }

    private static void throwFieldHasNoGenericPatamException(Class className, String fieldName) {
        String errorMessage = "В классе " + className.getSimpleName() + " поле " + fieldName + " не имеет generic параметризации";
        throw new HomeworkNotCorrectException(errorMessage);
    }
}
