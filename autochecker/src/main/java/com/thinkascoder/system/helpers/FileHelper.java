package com.thinkascoder.system.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileHelper {
    public static boolean deleteFileIfExists(File file) {
        return file.delete();
    }

    public static String readFromFile(File file) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return sb.toString();
    }
/*    public static File getFileFromResourcesFolder(String fileInResourcesw) {
        FileHelper.class.getClassLoader().getResource()
    }*/
}
