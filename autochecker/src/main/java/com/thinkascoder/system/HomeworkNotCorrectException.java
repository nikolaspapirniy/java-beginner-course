package com.thinkascoder.system;


public class HomeworkNotCorrectException extends RuntimeException {
    public HomeworkNotCorrectException(String message) {
        super(message);
    }
}

