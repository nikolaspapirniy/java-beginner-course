package com.thinkascoder.system.runner;

import com.thinkascoder.system.HandsOn;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.util.List;

public class HandsOnJUnitTestRunner extends BlockJUnit4ClassRunner {
    public HandsOnJUnitTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        return getTestClass().getAnnotatedMethods(HandsOn.class);
    }
}
