package com.thinkascoder.system.runner;

import com.thinkascoder.system.validations.Validations;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.nio.charset.Charset;

@RunWith(HandsOnJUnitTestRunner.class)
public abstract class HandsOnRunner extends Validations {
    // Что бы использовать кодировку utf-8
    static {
        System.setProperty("file.encoding", "UTF-8");
        Field charset = null;
        try {
            charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
