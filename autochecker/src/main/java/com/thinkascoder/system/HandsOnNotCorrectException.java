package com.thinkascoder.system;

public class HandsOnNotCorrectException extends RuntimeException {
    public HandsOnNotCorrectException(String message) {
        super(message);
    }
}
