package com.thinkascoder.system.validations.classes;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertInheritance;

public class AssesClassInheritedFromAnotherTest {
    class A {

    }

    class B extends A {

    }

    class B1 extends B {

    }

    class C {

    }

    @Test
    public void should_pass_if_class_inherited_from_another() throws Exception {
        assertInheritance(A.class, B.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_class_is_not_inherited_from_another() throws Exception {
        assertInheritance(A.class, C.class);
    }

    @Test
    public void should_pass_if_inherited_two_level_above() throws Exception {
        assertInheritance(A.class, B1.class);
    }
}
