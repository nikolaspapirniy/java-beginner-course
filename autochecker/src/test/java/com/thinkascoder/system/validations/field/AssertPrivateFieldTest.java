package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertPrivateField;

public class AssertPrivateFieldTest extends Validations {
    @Test
    public void should_pass_if_private_field_is_present() throws Exception {
        class PrivateField {
            private int a;
        }

        assertPrivateField(PrivateField.class, "a");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_with_wrong_modifier() throws Exception {
        class PrivateField {
            public int a;
        }

        assertPrivateField(PrivateField.class, "a");
    }
}
