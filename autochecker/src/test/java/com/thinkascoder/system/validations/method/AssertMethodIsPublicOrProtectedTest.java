package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodIsPublicOrProtected;

public class AssertMethodIsPublicOrProtectedTest {
    class A {
        public void publicMethod() {

        }

        private void privateMethod() {

        }

        protected void protectedMethod() {

        }

        void defaultMethod() {

        }
    }

    @Test
    public void should_pass_if_method_is_public() throws Exception {
        assertMethodIsPublicOrProtected(A.class, "publicMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_private() throws Exception {
        assertMethodIsPublicOrProtected(A.class, "privateMethod");
    }

    @Test
    public void should_pass_if_method_is_protected() throws Exception {
        assertMethodIsPublicOrProtected(A.class, "protectedMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_default() throws Exception {
        assertMethodIsPublicOrProtected(A.class, "defaultMethod");
    }
}
