package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodIsPublic;

public class AssertMethodIsPublicTest {
    class A {
        public void publicMethod() {

        }

        private void privateMethod() {

        }

        protected void protectedMethod() {

        }

        void defaultMethod() {

        }
    }

    @Test
    public void should_pass_if_method_is_public() throws Exception {
        assertMethodIsPublic(A.class, "publicMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_private() throws Exception {
        assertMethodIsPublic(A.class, "privateMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_protected() throws Exception {
        assertMethodIsPublic(A.class, "protectedMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_default() throws Exception {
        assertMethodIsPublic(A.class, "defaultMethod");
    }
}
