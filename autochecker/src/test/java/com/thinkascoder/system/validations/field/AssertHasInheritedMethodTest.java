package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertHasInheritedMethod;

public class AssertHasInheritedMethodTest {
    class A {
        private void privateMethod() {

        }

        public void publicMethod() {

        }
    }

    class B extends A {

    }

    @Test
    public void should_pass_if_method_inherited() throws Exception {
        assertHasInheritedMethod(B.class, "publicMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_not_reachable_in_inherited_class() throws Exception {
        assertHasInheritedMethod(B.class, "privateMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_no_method_in_parent_class() throws Exception {
        assertHasInheritedMethod(B.class, "no_such_method");
    }
}
