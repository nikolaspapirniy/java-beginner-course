package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertPublicField;


public class AssertPublicFieldTest extends Validations {
    @Test
    public void should_pass_if_public_field_is_present() throws Exception {
        class PublicField {
            public int a;
        }

        assertPublicField(PublicField.class, "a");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_with_wrong_modifier() throws Exception {
        class PublicField {
            private int a;
        }

        assertPublicField(PublicField.class, "a");
    }
}
