package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import java.util.Collection;

import static com.thinkascoder.system.validations.FieldValidations.assertFieldTypeWithGeneric;

public class AssertFieldTypeWithGenericTest {
    @Test
    public void should_pass_if_field_type_is_generic() throws Exception {
        class A<E> {
            Collection<E> collection;
        }

        assertFieldTypeWithGeneric(A.class, "collection");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_type_is_not_generic() throws Exception {
        class A<E> {
            Collection collection;
        }

        assertFieldTypeWithGeneric(A.class, "collection");
    }
}
