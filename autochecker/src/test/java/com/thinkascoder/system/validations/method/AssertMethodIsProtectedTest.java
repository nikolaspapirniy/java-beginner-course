package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodIsProtected;

public class AssertMethodIsProtectedTest {
    class A {
        public void publicMethod() {

        }

        private void privateMethod() {

        }

        protected void protectedMethod() {

        }

        void defaultMethod() {

        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_public() throws Exception {
        assertMethodIsProtected(A.class, "publicMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_private() throws Exception {
        assertMethodIsProtected(A.class, "privateMethod");
    }

    @Test
    public void should_pass_if_method_is_protected() throws Exception {
        assertMethodIsProtected(A.class, "protectedMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_default() throws Exception {
        assertMethodIsProtected(A.class, "defaultMethod");
    }
}
