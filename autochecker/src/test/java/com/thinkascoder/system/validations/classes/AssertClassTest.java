package com.thinkascoder.system.validations.classes;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertClass;

public class AssertClassTest {
    /*@Test
    public void should_pass_when_class_is_in_class_path() throws Exception {
        assertClass("AssertClassTest");
    }*/

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_when_class_is_not_in_class_path() throws Exception {
        assertClass("NoSuchClassss12313123123");
    }
}
