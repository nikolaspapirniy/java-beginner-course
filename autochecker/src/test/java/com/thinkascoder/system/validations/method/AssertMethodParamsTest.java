package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodParams;


public class AssertMethodParamsTest {
    @Test
    public void should_pass_if_method_with_no_params() throws Exception {
        class ClassWithMethodWithoutParams {
            void method() {

            }
        }

        assertMethodParams(ClassWithMethodWithoutParams.class, "method");
    }

    @Test
    public void should_pass_if_method_with_one_param() throws Exception {
        class ClassWithMethodWithOneParam {
            void method(int param) {

            }
        }

        assertMethodParams(ClassWithMethodWithOneParam.class, "method", int.class);
    }

    @Test
    public void should_pass_if_method_with_two_params() throws Exception {
        class ClassWithMethodWithTwoParams {
            void method(int param, double param2) {

            }
        }

        assertMethodParams(ClassWithMethodWithTwoParams.class, "method", int.class, double.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_param_type_is_wrong() throws Exception {
        class ClassWithMethod {
            void method(int param) {

            }
        }

        assertMethodParams(ClassWithMethod.class, "method", double.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_one_of_the_params_is_wrong() throws Exception {
        class ClassWithMethodWithTwoParams {
            void method(int param1, double param2) {

            }
        }

        assertMethodParams(ClassWithMethodWithTwoParams.class, "method", int.class, int.class);
    }
}
