package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertHasMethod;

public class AssertHasNonStaticMethodTest {
    @Test
    public void should_pass_if_method_with_any_modifier_present() throws Exception {
        class ClassWithMethods {
            public void publicMethod() {
            }

            private void privateMethod() {
            }

            protected void protectedMethod() {
            }

            void defaultMethod() {
            }
        }

        assertHasMethod(ClassWithMethods.class, "publicMethod");
        assertHasMethod(ClassWithMethods.class, "privateMethod");
        assertHasMethod(ClassWithMethods.class, "protectedMethod");
        assertHasMethod(ClassWithMethods.class, "defaultMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_absent() throws Exception {
        class ClassWithoutMethods {
        }

        assertHasMethod(ClassWithoutMethods.class, "noMethod");
    }

    static class ClassWithStaticMethod {
        public static void staticMethod() {

        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_static() throws Exception {
        assertHasMethod(ClassWithStaticMethod.class, "staticMethod");
    }
}
