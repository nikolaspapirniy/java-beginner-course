package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasInheritedField;


public class AssertHasInheritedFieldTest {
    class A {
        public String publicField;
        private String privateField;
        protected String protectedField;
    }

    class B extends A {
    }

    @Test
    public void should_pass_if_field_is_inherited() throws Exception {
        assertHasInheritedField(B.class, "publicField", String.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_is_private_and_not_inherited() throws Exception {
        assertHasInheritedField(B.class, "privateField", String.class);
    }

    @Test
    public void should_fail_if_field_is_protected_and_not_inherited() throws Exception {
        assertHasInheritedField(B.class, "protectedField", String.class);
    }
}
