package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertHasStaticMethod;

public class AssertHasStaticMethod {
    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_absent() throws Exception {
        class ClassWithoutMethods {
        }

        assertHasStaticMethod(ClassWithoutMethods.class, "noMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_not_static() throws Exception {
        class ClassWithStaticMethod {
            void nonStaticMethod() {

            }
        }

        assertHasStaticMethod(ClassWithStaticMethod.class, "nonStaticMethod");
    }
}
