package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodReturnsNothing;

public class AssertMethodReturnsNothingTest {
    @Test
    public void should_pass_if_method_returns_nothing() throws Exception {
        class CorrectClass {
            void method() {
            }
        }

        assertMethodReturnsNothing(CorrectClass.class, "method");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_returns_something() throws Exception {
        class CorrectClass {
            int method() {
                return 0;
            }
        }

        assertMethodReturnsNothing(CorrectClass.class, "method");
    }
}

