package com.thinkascoder.system.validations.constructor;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.ConstructorValidations.assertHasConstructor;

public class AssertHasConstructorTest {
    static class ClassWithConstructor {
        public ClassWithConstructor(int a) {
        }
    }

    @Test
    public void should_pass_if_constructor_with_one_param_present() throws Exception {
        assertHasConstructor(ClassWithConstructor.class, int.class);
    }

    static class ClassWithWrongType {
        public ClassWithWrongType(int a) {

        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_constructor_with_one_param_with_wrong_type_present() throws Exception {
        assertHasConstructor(ClassWithWrongType.class, double.class);
    }

    static class ClassWithTwoParamsCorrectTypes {
        public ClassWithTwoParamsCorrectTypes(int a, int b) {

        }
    }

    @Test
    public void should_pass_if_constructor_with_two_params_is_present() throws Exception {
        assertHasConstructor(ClassWithTwoParamsCorrectTypes.class, int.class, int.class);
    }


    static class ClassWithWrongTypeTwoParams {
        public ClassWithWrongTypeTwoParams(int a, long b) {
        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_constructor_with_two_params_is_present_but_one_with_wrong_type() throws Exception {
        assertHasConstructor(ClassWithWrongTypeTwoParams.class, int.class, int.class);
    }

    static class ClassWithoutConstructor {
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_no_constructor_present() throws Exception {
        assertHasConstructor(ClassWithWrongTypeTwoParams.class, int.class);
    }

    static class ClassWithMultipleConstructors {
        public ClassWithMultipleConstructors(double a) {

        }

        public ClassWithMultipleConstructors(int a) {
        }

        public ClassWithMultipleConstructors(char a) {
        }
    }

    @Test
    public void should_pass_if_constructor_with_one_param_present_in_any_order() throws Exception {
        assertHasConstructor(ClassWithMultipleConstructors.class, int.class);
    }
}
