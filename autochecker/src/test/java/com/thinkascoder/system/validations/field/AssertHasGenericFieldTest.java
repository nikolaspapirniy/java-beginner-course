package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasGenericField;

public class AssertHasGenericFieldTest {
    @Test
    public void should_pass_if_field_is_generic() throws Exception {
        class A<E> {
            private E field;
        }

        assertHasGenericField(A.class, "field");
    }

    @Test
    public void should_pass_if_field_is_array_and_generic() throws Exception {
        class A<E> {
            private E[] field;
        }

        assertHasGenericField(A.class, "field");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_is_not_generic() throws Exception {
        class A<E> {
            private int field;
        }

        assertHasGenericField(A.class, "field");
    }
}
