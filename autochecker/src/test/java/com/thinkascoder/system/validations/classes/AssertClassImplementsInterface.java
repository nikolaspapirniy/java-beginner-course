package com.thinkascoder.system.validations.classes;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertClassImplementsInterface;

public class AssertClassImplementsInterface {
    interface C {

    }

    class A implements C {
    }

    class B {

    }

    @Test
    public void should_pass_if_class_implements_interface() throws Exception {
        assertClassImplementsInterface(A.class, C.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_class_not_implements_interface() throws Exception {
        assertClassImplementsInterface(B.class, C.class);
    }
}
