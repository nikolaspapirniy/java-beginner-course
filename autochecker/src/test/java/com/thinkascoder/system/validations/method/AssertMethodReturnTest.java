package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodReturn;


public class AssertMethodReturnTest {
    @Test
    public void should_pass_if_method_returns_correct_type() throws Exception {
        class CorrectClass {
            int method() {
                return 0;
            }
        }

        assertMethodReturn(CorrectClass.class, "method", int.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_returns_wrong_type() throws Exception {
        class WrongClass {
            int method() {
                return 0;
            }
        }

        assertMethodReturn(WrongClass.class, "method", double.class);
    }

    @Test
    public void should_pass_if_method_returns_void() throws Exception {
        class CorrectClass {
            void method() {
            }
        }

        assertMethodReturn(CorrectClass.class, "method", void.class);
    }
}
