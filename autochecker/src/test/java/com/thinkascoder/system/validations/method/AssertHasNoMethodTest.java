package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertHasNoMethod;

public class AssertHasNoMethodTest {
    class A {
        public void publicMethod() {
        }

        private void privateMethod() {
        }

        protected void protectedMethod() {
        }

        void defaultMethod() {
        }
    }

    @Test
    public void should_pass_if_no_method() throws Exception {
        assertHasNoMethod(A.class, "no_method");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_public_method_present() throws Exception {
        assertHasNoMethod(A.class, "publicMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_private_method_present() throws Exception {
        assertHasNoMethod(A.class, "privateMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_protected_method_present() throws Exception {
        assertHasNoMethod(A.class, "protectedMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_default_method_present() throws Exception {
        assertHasNoMethod(A.class, "defaultMethod");
    }
}
