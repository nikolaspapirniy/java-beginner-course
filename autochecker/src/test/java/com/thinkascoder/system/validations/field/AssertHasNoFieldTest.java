package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasNoField;

public class AssertHasNoFieldTest extends Validations {
    @Test
    public void should_pass_if_field_is_not_present() throws Exception {
        class ClassWithoutField {
        }

        assertHasNoField(ClassWithoutField.class, "a");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_is_present() throws Exception {
        class ClassWithField {
            int a;
        }

        assertHasNoField(ClassWithField.class, "a");
    }
}
