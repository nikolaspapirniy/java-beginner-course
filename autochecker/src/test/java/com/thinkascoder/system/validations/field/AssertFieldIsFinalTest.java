package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertFinalField;

public class AssertFieldIsFinalTest {
    class Class {
        public final int finalField = 0;
        public int notFinalField = 0;
    }

    @Test
    public void should_pass_if_field_is_final() throws Exception {
        assertFinalField(Class.class, "finalField");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_field_is_final() throws Exception {
        assertFinalField(Class.class, "notFinalField");
    }
}
