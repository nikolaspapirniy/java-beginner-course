package com.thinkascoder.system.validations.method;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertMethodIsPrivate;

public class AssertMethodIsPrivateTest {
    class A {
        public void publicMethod() {

        }

        private void privateMethod() {

        }

        protected void protectedMethod() {

        }

        void defaultMethod() {

        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_public() throws Exception {
        assertMethodIsPrivate(A.class, "publicMethod");
    }

    @Test
    public void should_pass_if_method_is_private() throws Exception {
        assertMethodIsPrivate(A.class, "privateMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_protected() throws Exception {
        assertMethodIsPrivate(A.class, "protectedMethod");
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_method_is_default() throws Exception {
        assertMethodIsPrivate(A.class, "defaultMethod");
    }
}
