package com.thinkascoder.system.validations.field;

import com.thinkascoder.system.HomeworkNotCorrectException;
import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasField;

public class AssertHasFieldTest extends Validations {
    @Test
    public void should_pass_if_primitive_field_with_any_modifier_present() throws Exception {
        class ClassWithField {
            public int a;
            private int b;
            protected int c;
            int d;
        }

        assertHasField(ClassWithField.class, "a", int.class);
        assertHasField(ClassWithField.class, "b", int.class);
        assertHasField(ClassWithField.class, "c", int.class);
        assertHasField(ClassWithField.class, "d", int.class);
    }

    @Test
    public void should_pass_if_object_field_with_any_modifier_present() throws Exception {
        class ClassWithField {
            public String a;
            private String b;
            protected String c;
            String d;
        }

        assertHasField(ClassWithField.class, "a", String.class);
        assertHasField(ClassWithField.class, "b", String.class);
        assertHasField(ClassWithField.class, "c", String.class);
        assertHasField(ClassWithField.class, "d", String.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_primitive_field_absent() throws Exception {
        class EmptyClass {
        }

        assertHasField(EmptyClass.class, "a", String.class);
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_primitive_field_with_wrong_type() throws Exception {
        class EmptyClass {
            int a;
        }

        assertHasField(EmptyClass.class, "a", String.class);
    }
}
