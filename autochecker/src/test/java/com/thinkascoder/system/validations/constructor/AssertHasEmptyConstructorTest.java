package com.thinkascoder.system.validations.constructor;

import com.thinkascoder.system.HomeworkNotCorrectException;
import org.junit.Test;

import static com.thinkascoder.system.validations.ConstructorValidations.assertHasDefaultConstructor;

public class AssertHasEmptyConstructorTest {
    static class ClassWithEmptyConstructor {
    }

    @Test
    public void should_pass_if_empty_constructor_is_by_default_present() throws Exception {
        assertHasDefaultConstructor(ClassWithEmptyConstructor.class);
    }

    static class ClassWithExplicitEmptyConstructor {
        public ClassWithExplicitEmptyConstructor() {

        }
    }

    @Test
    public void should_pass_if_empty_constructor_is_explicit() throws Exception {
        assertHasDefaultConstructor(ClassWithEmptyConstructor.class);
    }

    static class ClassWithoutEmptyConstructor {
        public ClassWithoutEmptyConstructor(int a) {

        }
    }

    @Test(expected = HomeworkNotCorrectException.class)
    public void should_fail_if_empty_constructor_is_not_present() throws Exception {
        assertHasDefaultConstructor(ClassWithoutEmptyConstructor.class);
    }
}
