package com.thinkascoder.basic.week2.homework._6;


/**
 * Программа по определению ялвяется ли число четное или не четное
 *
 * 1. Вам нужно написать реализацию одного теста в файле IsOddOrEvenTest (src/test/java/com.thinkascoder.basic.week2.homework)
 * (начните с isEven_should_say_when_number_is_even)
 * 2. Запустить тест и убедиться что он красный
 * 3. Реализовать работу метода (например для начала isEven) что бы тест стал зеленым
 * 4. Перейти к пункту 1, пока все тесты не будут написаны и все не будут проходить
 *
 * Пример работы программы(вызов из кода):
 * IsOddOrEven.isEven(2) -> true
 * IsOddOrEven.isEven(1) -> false
 * IsOddOrEven.isEven(0) -> true
 *
 * IsOddOrEven.isOdd(2) -> false
 * IsOddOrEven.isOdd(1) -> true
 * IsOddOrEven.isOdd(0) -> false
 *
 * Как сделать лучше? Реализовать каждый метод в 1 строку
 */
public class IsOddOrEven {
    public static boolean isEven(int number) {
        return false;
    }

    public static boolean isOdd(int number) {
        return false;
    }
}
