package com.thinkascoder.basic.week2.homework._8;

/**
 * Программа по определению принадлежит ли число интервалу
 *
 * Acceptance criteria(условия принятия работы):
 * 1. Всем методы в классе IntervalChecker реализованы
 * 2. Все тесты в IntervalCheckerTest (src/test/java/com.thinkascoder.basic.week2.homework) проходят
 * 3. В файле IntervalCheckerTest 8 тестов - 4 для isInIntervalExclusive и 4 для isInIntervalInclusive
 *
 * Пример работы программы(вызов из кода):
 * IntervalChecker.isInIntervalExclusive(1, 10, 5) вернет true
 * IntervalChecker.isInIntervalExclusive(1, 10, 100) вернет false
 * IntervalChecker.isInIntervalExclusive(1, 10, 1) вернет false
 * IntervalChecker.isInIntervalExclusive(1, 10, 10) вернет false
 *
 * IntervalChecker.isInIntervalInclusive(1, 10, 5) вернет true
 * IntervalChecker.isInIntervalInclusive(1, 10, 100) вернет false
 * IntervalChecker.isInIntervalInclusive(1, 10, 1) вернет true
 * IntervalChecker.isInIntervalInclusive(1, 10, 10) вернет true
 *
 * Как сделать лучше? Реализовать каждый метод в 1 строку
 */
public class IntervalChecker {
    /**
     * @return true если число в интервале (from, end), не включая границы
     */
    public static boolean isInIntervalExclusive(int from, int end, int number) {
        return false;
    }

    /**
     * @return true если число в интервале [from, end], включая границы
     */
    public static boolean isInIntervalInclusive(int from, int end, int number) {
        return false;
    }
}
