package com.thinkascoder.basic.week2.homework._7;

/**
 * Программа простой калькулятор
 *
 * Acceptance criteria(условия принятия работы):
 * 1. Все методы в Calculator реализованы
 * 2. Все тесты в CalculatorTest (src/test/java/com.thinkascoder.basic.week2.homework) проходят
 * 3. В CalculatorTest 21 тест соответствующий спецификации(вверху CalculatorTest)
 */
public class Calculator {
    public static long add(long first, long second) {
        return 0;
    }

    public static long sub(long first, long second) {
        return 0;
    }

    public static long mul(long first, long second) {
        return 0;
    }

    public static double div(long first, long second) {
        return 0;
    }
}
