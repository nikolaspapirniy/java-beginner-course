package com.thinkascoder.basic.week4.homework;

public class FieldChecker {
    public static boolean isEmpty(char[][] field, int row, int col) {
        return false;
    }

    public static boolean checkRow(char[][] field, int row) {
        return false;
    }

    public static boolean checkColumn(char[][] field, int column) {
        return false;
    }

    public static boolean checkLeftDiagonal(char[][] field) {
        return false;
    }

    public static boolean checkRightDiagonal(char[][] field) {
        return false;
    }

    public static boolean checkField(char[][] field) {
        return false;
    }
}
