package com.thinkascoder.basic.week3.homework._6;

/**
 * Реализовать методы и написать минимум по 2 теста на каждый метод
 * (src/test/java/com.thinkascoder.basic.week3.homework._6.LoopsTasksTest)
 */
public class LoopsTasks {
    /**
     * Сумма значений до value
     * Например: 
     * при value = 3, ответ = 6
     * при value = -3, ответ = -6
     */
    public static int sumOfValues(int value) {
        return 0;
    }

    /**
     * Метод суммирует нечетные значения до value включительно
     * Например: 
     * при value = 3, ответ = 4(1 и 3)
     * при value = -3, ответ = -4(-1 и -3)
     */
    public static int countOfOddValues(int value) {
        return 0;
    }

    /**
     * Метод суммирует четные значения до value включительно
     * Например при value = 3, ответ = 2 (помни про вариант с отрицательными значениями)
     */
    public static int countOfEvenValues(int value) {
        return 0;
    }

    /**
     * Сколько цифр в числе?
     * (нужно посчитать количество с помощью арифметических операций - не используя приведение числа к строке)
     * Примеры: 
     * 1. В числе 100500, 6 цифр
     * 2. В числе -100500, 6 цифр (минус не учитывается)
     */
    public static int countOfDigits(int value) {
        return 0;
    }

    /**
     * Вывести треугольник
     * Подсказка: перевод строки - "\n"
     * Пример: lines = 5
     * Вывод:
     *
     * *
     * **
     * ***
     * ****
     * *****
     * А что если lines будет < 0?
     */
    public static String printTriangle(int lines) {
        return "";
    }

    /**
     * Вывести треугольник
     * Подсказка: перевод строки - "\n"
     * Пример: lines = 5
     * Вывод:
     *
     *     *
     *    ***
     *   *****
     *  *******
     * *********
     */
    public static String printNewYearsTree(int lines) {
        return "";
    }

    /**
     * Вывести сообщение в рамочке
     * Подсказки: перевод строки - "\n"
     * Пример: text = hello
     * Вывод:
     * |-------|
     * | Hello |
     * |-------|
     */
    public static String printInFrame(String text) {
        return "";
    }
}
