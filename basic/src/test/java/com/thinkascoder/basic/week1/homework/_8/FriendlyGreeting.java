package com.thinkascoder.basic.week1.homework._8;

import java.util.Scanner;

/**
 * Программа привествия пользователя (решение в методе main)
 * <p>
 * 1. Вам нужно попросить пользователя ввести свое имя
 * 2. После этого, нужно вывести "Привет {имя которое он ввел}!"
 * <p>
 * Пример работы программы:
 * > Введите пожалуйста ваше имя:
 * > Сергей
 * > Привет Сергей!
 */
public class FriendlyGreeting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Напишите решение ниже
    }
}
