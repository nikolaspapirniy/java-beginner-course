package com.thinkascoder.basic.week1.handsOn._6;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из шестнацитеричной системы исчисления в десятичную
 * важно расписать каждый шаг
 */
public class From16To10 extends HandsOnRunner {
    /**
     * Пример:
     * Перевести число FF
     * FF = (15 * 16^1) + (15 * 16^0) = 240 + 15 = 255
     */
    @HandsOn
    public void example_translate_to_dec_FF() {
        assertEquals(Integer.toString(0xFF, 10), "255");
        // Заметили? Числовые литералы в шестнадцатеричной системе пишутся начиная с 0x
    }

    /**
     * Перевести число FF
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_dec_1BB() {
        assertEquals(Integer.toString(0x1BB, 10), ___);
    }

    /**
     * Перевести число FF
     */
    @HandsOn
    public void translate_to_dec_FFF() {
        assertEquals(Integer.toString(0xFFF, 10), ___);
    }

    /**
     * Перевести число BAAC
     */
    @HandsOn
    public void translate_to_dec_BAAC() {
        assertEquals(Integer.toString(0xBAAC, 10), ___);
    }

    /**
     * Перевести число ABCFC
     */
    @HandsOn
    public void translate_to_dec_ABCFC() {
        assertEquals(Integer.toString(0xABCFC, 10), ___);
    }
}
