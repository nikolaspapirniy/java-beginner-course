package com.thinkascoder.basic.week1.handsOn._2;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class MathHandsOn extends HandsOnRunner {
    @HandsOn("Простые математические операторы работают именно так как ты думаешь")
    public void simple_operators_are_obvious() throws Exception {
        assertEquals(2 + 2, ___);
        assertEquals(2 * 2, ___);
        assertEquals(10 / 2, ___);
        assertEquals(5 - 2, ___);
    }

    @HandsOn("Знак % возвращает остаток отделения")
    public void modulo_operator_returns_modulo() throws Exception {
        assertEquals(10 % 3, ___);
    }

    @HandsOn("С помощью остатка от деления можно определить является ли число четным или нечетным")
    public void you_can_check_if_values_is_odd_or_even_with_modulo() throws Exception {
        assertEquals(1 % 2, ___);
        assertEquals(2 % 2, ___);
        assertEquals(3 % 2, ___);
        assertEquals(4 % 2, ___);
        assertEquals(5 % 2, ___);
    }

    @HandsOn("А что вернет такая операция? Почему?")
    public void think_about_this_example_can_you_prove_that_its_correct() throws Exception {
        assertEquals(1 % 16, ___);
    }

    @HandsOn("В библиотеке java есть встроенный набор тригонометрических функций")
    public void java_math_lib_has_trigonometry_functions() throws Exception {
        assertEquals(Math.sin(0.0), ___);
        assertEquals(Math.cos(0.0), ___);
        assertEquals(Math.tan(0.0), ___);
    }

    @HandsOn("Еще есть математические операции возведение в степень и корень")
    public void java_math_lib_also_has_pow_and_sqrt_operations() throws Exception {
        assertEquals(Math.pow(5, 2), ___);
        assertEquals(Math.pow(2, 3), ___);

        assertEquals(Math.sqrt(49), ___);
    }

    @HandsOn("Округление чилса имеет несколько вариантов. В чем разница?")
    public void you_can_also_round_doubles_by_multiple_ways() throws Exception {
        assertEquals(Math.floor(5.6), ___);
        assertEquals(Math.ceil(5.6), ___);
        assertEquals(Math.round(5.6), ___);
    }

    @HandsOn("Поиск минимума и максимума тоже реализован")
    public void min_and_max_has_never_been_so_easy_to_find() throws Exception {
        assertEquals(Math.min(10, 20), ___);
        assertEquals(Math.max(100, 25), ___);
    }

    @HandsOn("Так же есть определение значения по модулю")
    public void absolute_values_also_present() throws Exception {
        assertEquals(Math.abs(-5), ___);
        assertEquals(Math.abs(5), ___);
    }
}
