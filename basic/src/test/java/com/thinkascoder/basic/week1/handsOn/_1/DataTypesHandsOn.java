package com.thinkascoder.basic.week1.handsOn._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Варианты типов данных:
 * <p>
 * boolean.class
 * byte.class
 * short.class
 * char.class
 * int.class
 * long.class
 * float.class
 * double.class
 * String.class
 */
public class DataTypesHandsOn extends HandsOnRunner {
    @HandsOn("литерал int используется чтобы хранить целые числа")
    public void int_can_be_used_to_hold_integers() {
        assertEquals(getType(1), ___); // подсказка: int.class
    }

    @HandsOn("Сколько бит в переменной типа int?")
    public void how_many_bits_in_int() {
        assertEquals(Integer.SIZE, ___);
    }

    @HandsOn("Какие максимальные и минимальные значения int?")
    public void what_are_max_and_min_values_of_int() {
        assertEquals(Integer.MIN_VALUE, ___);
        assertEquals(Integer.MAX_VALUE, ___);
    }

    @HandsOn("Если число очень большое нужно использовать тип long")
    public void numbers_can_be_bigger_in_long_with_L() {
        assertEquals(getType(1_000_000_000L), ___);
    }

    @HandsOn("Большие числа можно писать целиком, а можно разделять нижним подчеркиванием")
    public void number_with_separated_by_underline_are_regular_numbers() {
        assertEquals(1_000_000, ___);
    }

    @HandsOn("Сколько бит в переменной типа long?")
    public void size_of_long_in_bits() {
        assertEquals(Long.SIZE, ___);
    }

    @HandsOn("Какие минимальные и максимальные значения типа long")
    public void min_and_max_values_of_long() {
        assertEquals(Long.MIN_VALUE, ___);
        assertEquals(Long.MAX_VALUE, ___);
    }

    @HandsOn("Для небольшим значений можно использовать тип short")
    public void variables_can_have_type_short() {
        short value = (short) 1;
        assertEquals(getType(value), ___);
    }

    @HandsOn("Сколько бит в переменной типа short?")
    public void size_of_short_in_bits() {
        assertEquals(Short.SIZE, ___);
    }

    @HandsOn("Какие минимальные и максимальные значения типа short")
    public void min_and_max_values_of_short() {
        assertEquals(Short.MIN_VALUE, ___);
        assertEquals(Short.MAX_VALUE, ___);
    }

    @HandsOn("Для переменных размером в 1 байт используется тип byte")
    public void variables_can_have_type_byte() {
        byte value = (byte) 1;
        assertEquals(getType(value), ___);
    }

    @HandsOn("Сколько бит в переменной типа byte?")
    public void size_of_byte_in_bits() {
        assertEquals(Byte.SIZE, ___);
    }

    @HandsOn("Какие минимальные и максимальные значения типа byte?")
    public void min_and_max_values_of_byte() {
        assertEquals(Byte.MIN_VALUE, ___);
        assertEquals(Byte.MAX_VALUE, ___);
    }

    @HandsOn("Для хранения символов используют тип char")
    public void variables_can_have_type_char() {
        assertEquals(getType('c'), ___);
    }

    @HandsOn("Значения символов храняться в виде целого числа")
    public void char_is_decimal_so_it_can_be_int() {
        int var = 'c';
        assertEquals(getType(var), ___);
    }

    @HandsOn("Сколько бит в переменной типа char?")
    public void size_of_char_in_bits() {
        assertEquals(Character.SIZE, ___);
    }

    @HandsOn("Какие минимальные и максимальные значения типа char?")
    public void since_char_is_decimal_too_it_has_min_and_max_values() {
        assertEquals(Character.MIN_VALUE, ___);
        assertEquals(Character.MAX_VALUE, ___);
    }

    @HandsOn("Как вы заметили переменные типа char хранят только положительные значения")
    public void as_you_may_noticed_char_holds_only_positive_numbers() {
        char negativeValueInChar = (char) (-1);
        int charAsInt = negativeValueInChar;

        assertEquals(charAsInt, ___);
    }

    @HandsOn("Тип double может хранить дробные значения")
    public void double_holds_decimal_numbers() {
        assertEquals(getType(3.14), ___);
    }

    @HandsOn("Сколько бит в переменной типа double?")
    public void double_size_in_bits() {
        assertEquals(Double.SIZE, ___);
    }

    @HandsOn("Тип float хранит дробные значения поменьше")
    public void float_also_holds_decimal_numbers_but_requires_F() {
        assertEquals(getType(3.14F), ___);
    }

    @HandsOn("Сколько бит в переменной типа float?")
    public void how_many_bits_in_float() {
        assertEquals(Float.SIZE, ___);
    }

    @HandsOn("Дробные значения float можно обьявлять с помощью экспоненты")
    public void floats_can_be_declared_with_exponents() {
        assertEquals(getType(1e3f), ___);
        assertEquals(1.0e3f, ___);
        assertEquals(1E3f, ___);
    }

    @HandsOn("Тип boolean хранит значения всего в 2 вариантах истина или ложь")
    public void boolean_used_for_true_false() throws Exception {
        boolean trueValue = true;
        boolean falseValue = false;

        assertEquals(trueValue, ___);
        assertEquals(falseValue, ___);
    }
}
