package com.thinkascoder.basic.week1.handsOn._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.text.MessageFormat;

public class StringsHandsOn extends HandsOnRunner {
    @HandsOn("Строки могут быть использованы как литералы")
    public void strings_could_be_expressed_as_literals() throws Exception {
        assertEquals(getType("i'm a string"), ___); // hint: String.class
    }

    @HandsOn("Сравнивать 2 строки нужно с помощью метода equals")
    public void strings_should_be_compared_by_equals_method() throws Exception {
        assertEquals("one".equals("one"), ___);
    }

    @HandsOn("Создание строки через new String(), это лишнее действие")
    public void strings_creation() throws Exception {
        String emptyString = new String();
        String betterWayToCreateEmptyString = "";

        assertEquals(emptyString.equals(betterWayToCreateEmptyString), ___);
    }

    @HandsOn("Переменные типа строка можно склеивать")
    public void string_concatenation_from_multiple_strings() throws Exception {
        String name = "Jim";
        String space = " ";
        String lastName = "Brown";

        assertEquals(name + space + lastName, ___);
    }

    @HandsOn("Так же можно склеивать строковые литералы")
    public void simpler_string_concatenation() throws Exception {
        assertEquals("Jim" + " " + "Brown", ___);
    }

    @HandsOn("Можно конструировать строки передавая параметры в шаблон")
    public void string_formatting_via_strings_format_method() throws Exception {
        assertEquals(String.format("%s %s", "Jim", "Brown"), ___);
    }

    @HandsOn("Параметры могут иметь номера")
    public void string_formatting_via_messageFormat_method() throws Exception {
        assertEquals(MessageFormat.format("{0} {1} {0}", "a", "b", "c"), ___);
    }

    @HandsOn("Узнать длинну строки можно через метод length")
    public void you_can_find_string_length_via_method_length() throws Exception {
        assertEquals("just a string".length(), ___);
    }

    @HandsOn("Конкретный символ можно получить с помощью метода charAt")
    public void specific_character_could_be_received_by_charAt_method() throws Exception {
        assertEquals("just a string".charAt(5), ___);
    }

    @HandsOn("Как вы заметили нумерация символов начинается с 0")
    public void charAt_is_starts_with_zero() throws Exception {
        assertEquals("just a string".charAt(0), ___);
    }

    @HandsOn("Значение какого типа данных возвращается из метода charAt?")
    public void what_type_does_charAt_returns() throws Exception {
        assertEquals(getType("just a string".charAt(0)), ___);
    }

    @HandsOn("Индекс последнего элемента на 1 меньше размера строки")
    public void get_last_character() throws Exception {
        assertEquals("hello".charAt(4), ___);
    }

    @HandsOn("Вы можете привести все символы строки к верхнему регистру")
    public void you_can_make_all_letters_upper_case() throws Exception {
        assertEquals("Ho ho ho".toUpperCase(), ___);
    }

    @HandsOn("Так же можно привести к нижнему регистру")
    public void you_can_make_all_letters_lower_case() throws Exception {
        assertEquals("Ho ho ho".toLowerCase(), ___);
    }

    @HandsOn("Можно легко убрать пробелы в начале и в конце")
    public void you_can_trim_string() throws Exception {
        assertEquals(" Hello".trim(), ___);
        assertEquals("Hello ".trim(), ___);
        assertEquals(" Hello ".trim(), ___);
    }
}
