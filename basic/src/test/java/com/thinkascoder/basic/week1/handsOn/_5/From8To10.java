package com.thinkascoder.basic.week1.handsOn._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из восьмеричной системы исчисления в десятичную,
 * важно расписать каждый шаг
 */
public class From8To10 extends HandsOnRunner {
    /**
     * Пример:
     * Перевести число 57
     * Распишите решение ниже:
     * 57 = (5 * 8^1) + (7 * 8^0) = 40 + 7 = 47
     */
    @HandsOn
    public void example_translate_to_decimal_57() {
        assertEquals(Integer.toString(057, 10), "47");
        // Заметили? Числовые литералы в восьмеричной системе начинаются с 0
    }

    /**
     * Перевести число 256
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_decimal_256() {
        assertEquals(Integer.toString(0256, 10), ___);
    }

    /**
     * Перевести число 1000
     */
    @HandsOn
    public void translate_to_decimal_1000() {
        assertEquals(Integer.toString(01000, 10), ___);
    }

    /**
     * Перевести число 3235
     */
    @HandsOn
    public void translate_to_decimal_3235() {
        assertEquals(Integer.toString(03235, 10), ___);
    }

    /**
     * Перевести число 3235
     */
    @HandsOn
    public void translate_to_decimal_7235() {
        assertEquals(Integer.toString(07235, 10), ___);
    }
}
