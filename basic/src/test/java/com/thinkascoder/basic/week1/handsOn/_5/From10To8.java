package com.thinkascoder.basic.week1.handsOn._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из десятичной системы исчисления в восьмеричную,
 * важно расписать каждый шаг
 */
public class From10To8 extends HandsOnRunner {
    /**
     * Пример:
     * <p>
     * Перевести число 31
     * 31 / 8 = 3 (7)
     * 3 / 8 = 0 (3)
     */
    @HandsOn
    public void example_translate_to_octal_31() {
        assertEquals(Integer.toString(31, 8), "37");
    }

    /**
     * Перевести число 333
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_octal_333() {
        assertEquals(Integer.toString(333, 8), ___);
    }

    /**
     * Перевести число 437
     */
    @HandsOn
    public void translate_to_octal_437() {
        assertEquals(Integer.toString(437, 8), ___);
    }

    /**
     * Перевести число 1578
     */
    @HandsOn
    public void translate_to_octal_1578() {
        assertEquals(Integer.toString(1578, 8), ___);
    }


    /**
     * Перевести число 2096
     */
    @HandsOn
    public void translate_to_octal_2096() {
        assertEquals(Integer.toString(2096, 8), ___);
    }
}
