package com.thinkascoder.basic.week1.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из двоичной системы исчисления в десятичную,
 * важно расписать каждый шаг
 */
public class From2To10 extends HandsOnRunner {

    /**
     * Пример:
     * Перевести число 111
     * 111 = (1*2^2) + (1*2^1) + (1*2^0) = 4 + 2 + 1 = 7
     */
    @HandsOn
    public void example_translate_to_binary_111() {
        assertEquals(Integer.toString(0b111, 10), "7");
        // Заметили? Числовые литералы в двоичной системе пишутся начиная с 0b
    }

    /**
     * Перевести число 1101
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_decimal_1101() {
        assertEquals(Integer.toString(0b1101, 10), ___);
    }

    /**
     * Перевести число 10011
     */
    @HandsOn
    public void translate_to_decimal_10011() {
        assertEquals(Integer.toString(0b10011, 10), ___);
    }

    /**
     * Перевести число 1010101110101
     */
    @HandsOn
    public void translate_to_decimal_1010101110101() {
        assertEquals(Integer.toString(0b1010101110101, 10), ___);
    }

    /**
     * Перевести число 1111111100001
     */
    @HandsOn
    public void translate_to_decimal_1111111100001() {
        assertEquals(Integer.toString(0b1111111100001, 10), ___);
    }
}
