package com.thinkascoder.basic.week1.handsOn._7;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class NumberNotationsHandsOn extends HandsOnRunner {
    @HandsOn("Числовые литералы в двоичной системе исчесления начинается с 0b")
    public void binary_literals_starts_with_0b() throws Exception {
        assertEquals(0b1010, ___); // подсказка: чему будет равно значение в десятичной системе
    }

    @HandsOn("Какой тип у значения бинарного литерала?")
    public void binary_literals_type() throws Exception {
        assertEquals(getType(0b1010), ___);
    }

    @HandsOn("Есть удобный способ выводить число в двоичной системе")
    public void print_binary_literals_as_string() throws Exception {
        assertEquals(Integer.toString(0b1010, 2), ___);
    }

    @HandsOn("Можно производить вычисления с бинарными литералами")
    public void you_can_do_math_on_literals_base_two() throws Exception {
        assertEquals(0b1010 + 0b111, ___);
    }

    @HandsOn("Числовые литералы в восьмеричной системе исчесления начинается с 0")
    public void octal_literals_starts_with_0() throws Exception {
        assertEquals(0712, ___);
    }

    @HandsOn("Какой тип у значения восьмеричного литерала?")
    public void octal_literals_type() throws Exception {
        assertEquals(getType(051), ___);
    }

    @HandsOn("Можно производить вычисления с литералами в восьмеричной системе исчисления")
    public void math_on_octal_literals() throws Exception {
        assertEquals(0572 * 0145, ___);
    }

    @HandsOn("Выводить восьмеричные литералы на экран, можно так же как и бинарные")
    public void print_octal_literals_as_string() throws Exception {
        assertEquals(Integer.toString(0572, 8), ___);
    }

    @HandsOn("Числовые литералы в шестнадцатеричной системе исчесления начинается с 0x")
    public void hex_literals_starts_with_0x() throws Exception {
        assertEquals(0xFF, ___);
    }

    @HandsOn("Какой тип у значения шестнадцатеричного литерала?")
    public void hex_literals_type() throws Exception {
        assertEquals(getType(0xAF), ___);
    }

    @HandsOn("А у большого шестнадцатеричного литерала?")
    public void which_type_of_big_hex_literals() throws Exception {
        assertEquals(getType(0xFFFFFFFFFFFFFFFL), ___); // вы заметили L в конце литерала? Для чего она там?
    }

    @HandsOn("Выводить шестнадцатеричные литералы на экран, можно так же")
    public void print_hex_literals_as_string() throws Exception {
        assertEquals(Integer.toString(0xFFFF, 16), ___);
    }

    @HandsOn("Более того можно вывести числа в любой системе исчисления до Character.MAX_RADIX")
    public void you_can_print_numbers_with_any_radix() throws Exception {
        // Естественно можно просто запустить код и посмотреть результат вычислений
        assertEquals(Integer.toString(100500, 7), ___);
        assertEquals(Integer.toString(100500, 14), ___);
        assertEquals(Integer.toString(100500, 28), ___);
        assertEquals(Integer.toString(100500, 30), ___);
        assertEquals(Character.MAX_RADIX, ___);
    }
}
