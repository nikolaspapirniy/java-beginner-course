package com.thinkascoder.basic.week1.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из десятичной системы исчисления в двоичную,
 * важно расписать каждый шаг
 */
public class From10To2 extends HandsOnRunner {
    /**
     * Пример:
     * Перевести число 7
     * 7 / 2 = 3 (1)
     * 3 / 2 = 1 (1)
     * 1 / 2 = 0 (1)
     */
    @HandsOn
    public void example_translate_to_binary_7() {
        assertEquals(Integer.toString(7, 2), "111");
        // Заметили? Integer.toString переводит любое число в любую систему исчисления
    }

    /**
     * Перевести число 122
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_binary_122() {
        assertEquals(Integer.toString(122, 2), ___);
    }


    /**
     * Перевести число 254
     */
    @HandsOn
    public void translate_to_binary_254() {
        assertEquals(Integer.toString(254, 2), ___);
    }

    /**
     * Перевести число 1024
     */
    @HandsOn
    public void translate_to_binary_1024() {
        assertEquals(Integer.toString(1024, 2), ___);
    }

    /**
     * Перевести число 5281
     */
    @HandsOn
    public void translate_to_binary_5281() {
        assertEquals(Integer.toString(5281, 2), ___);
    }
}
