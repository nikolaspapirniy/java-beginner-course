package com.thinkascoder.basic.week1.handsOn._6;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Рассписать процесс перевода числа из десятичной системы исчисления в шестнадцатеричную
 * важно расписать каждый шаг
 */
public class From10To16 extends HandsOnRunner {

    /**
     * Пример:
     * Перевести число 31
     * 31 / 16 = 1 (15 => f)
     * 1 / 16 = 0 (1 => 1)
     */
    @HandsOn
    public void example_translate_to_hex_31() {
        assertEquals(Integer.toString(31, 16), "1f");
    }

    /**
     * Перевести число 316
     * Распишите решение ниже:
     */
    @HandsOn
    public void translate_to_hex_316() {
        assertEquals(Integer.toString(316, 16), ___);
    }

    /**
     * Перевести число 1024
     */
    @HandsOn
    public void translate_to_hex_1024() {
        assertEquals(Integer.toString(1024, 16), ___);
    }

    /**
     * Перевести число 5212
     */
    @HandsOn
    public void translate_to_hex_5212() {
        assertEquals(Integer.toString(5212, 16), ___);
    }

    /**
     * Перевести число 5212
     */
    @HandsOn
    public void translate_to_hex_4212() {
        assertEquals(Integer.toString(4212, 16), ___);
    }
}
