package com.thinkascoder.basic.week4.homework._7;

/**
 * Пользователи будут вводить строку и колонку, куда нужно поставить крестик или нолик поочереди
 * Например:
 * 0 0
 * 1 2
 * <p>
 * Методы в UserInput(src/main/java/com.thinkascoder.basic.week4.homework)
 * помогут обрабатывать пользовательскую строку
 * то есть:
 * parseInputString - бьет строку на несколько(например "1 2" -> ["1", "2"])
 * Подсказка: в случае ошибки можно вернуть null, а дальше проверить if(result == null)
 * getRow - (например ["1", "2"] -> 1)
 * getCol - (например ["1", "2"] -> 2)
 * <p>
 * Напишите тесты и реализуйте метод
 */
public class UserInputTest {
}
