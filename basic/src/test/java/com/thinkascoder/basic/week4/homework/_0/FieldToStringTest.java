package com.thinkascoder.basic.week4.homework._0;

import com.thinkascoder.basic.week4.homework.GameHelper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Наше поле в крестиках-ноликах будет представлять из себя char[][]
 * (любого размера, но для примера возьмем 3х3)
 * Где:
 * - пустое поле = '-'
 * - крестик = 'x'
 * - нолик = 'o'
 * <p>
 * Пример поля
 * {'o', 'x', '-'},
 * {'-', 'x', 'o'},
 * {'o', 'x', '-'}
 * <p>
 * Для начала нам нужен метод GameHelper.toString(src/main/java/com.thinkascoder.basic.week4.homework)
 * который будет выводить наше поле на экран
 * Пример:
 * {'o', 'x', '-'},
 * {'-', 'x', 'o'},
 * {'o', 'x', '-'}
 * в виде строки будет:
 * <p>
 * o | x | -
 * ---------
 * - | x | o
 * ---------
 * o | x | -
 * <p>
 * Какие тесты стоит написать?
 * <p>
 * Напишите тесты и реализуйте метод
 */
public class FieldToStringTest {
    @Test
    public void should_not_print_empty_array() throws Exception {
        char[][] emptyField = new char[0][0];
        String result = GameHelper.toString(emptyField);
        assertEquals("", result);
    }
}
