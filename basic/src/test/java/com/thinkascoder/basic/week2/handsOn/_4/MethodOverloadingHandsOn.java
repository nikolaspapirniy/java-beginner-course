package com.thinkascoder.basic.week2.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class MethodOverloadingHandsOn extends HandsOnRunner {

    public static int triple(int a) {
        return a * a * a;
    }

    public static double triple(double a) {
        return a * a * a;
    }

    public static String fractionToString(int a) {
        return a + " / " + 1;
    }

    public static String fractionToString(int a, int b) {
        return a + " / " + b;
    }

    @HandsOn("Методы могут иметь одинаковое имя, но разный тип параметров - это называется перегрузка метода")
    public void overloaded_methods_may_have_same_name_but_different_parameter_types() throws Exception {
        assertEquals(triple(2), ___);
        assertEquals(triple(1.5), ___);
        // Почему код ниже не скомпилируется?
        // assertEquals(triple(""), ___);
    }

    @HandsOn("Перегруженые методы могут так же отличаться набором параметров")
    public void overloaded_methods_may_have_same_name_but_differ_by_argument_list() throws Exception {
        assertEquals(fractionToString(3), ___);
        assertEquals(fractionToString(1, 2), ___);
    }

    @HandsOn("Перегруженые методы не могут отличаться лишь типом возвращаемого значения, будет ошибка компиляции")
    public void overloaded_methods_may_() throws Exception {
        fail("Раскоментируй код ниже чтобы убедиться что оба метода расценены как 1, они не перегружены");
    }

    /*
    public static String method() {
        return "0";
    }

    public static int method() {
        return 0;
    }
    */
}
