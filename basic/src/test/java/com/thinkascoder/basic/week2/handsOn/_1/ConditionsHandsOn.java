package com.thinkascoder.basic.week2.handsOn._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Для лучшего эффекта сначала реши задачу на листике, а потом запускай HandsOn
 */
public class ConditionsHandsOn extends HandsOnRunner {
    @HandsOn("Покажите как вы поняли логическое и")
    public void logical_and_is_true_when_both_are_true() throws Exception {
        assertEquals(false && false, ___);
        assertEquals(false && true, ___);
        assertEquals(true && false, ___);
        assertEquals(true && true, ___);
    }

    @HandsOn("Покажите как вы поняли логическое или")
    public void logical_or_is_true_when_one_is_true() throws Exception {
        assertEquals(false || false, ___);
        assertEquals(false || true, ___);
        assertEquals(true || false, ___);
        assertEquals(true || true, ___);
    }

    @HandsOn("А что такое логическое отрицание?")
    public void logical_not_is_true_when_false() throws Exception {
        assertEquals(!false, ___);
        assertEquals(!true, ___);
    }

    @HandsOn("Подумайте какой будет результат у этих логических утверждений")
    public void combination_of_logical_operators() throws Exception {
        assertEquals(!(!true && !false), ___);
        assertEquals(true || false && false, ___);
        assertEquals(((true && false) || !false), ___);
    }

    @HandsOn("Еще немного сложных логических задачек")
    public void more_logical_tasks() throws Exception {
        boolean condition1 = false;
        boolean condition2 = true;
        boolean condition3 = condition1 || condition2;

        assertEquals(condition1 && condition3 || condition2, ___);
        assertEquals(!(condition1) && condition3 && condition1 || !condition2, ___);
        assertEquals(condition1 || !condition1 && (condition1 && condition1), ___);
    }
}
