package com.thinkascoder.basic.week2.homework._9;

/**
 * Программа калькулятор для десятичных чисел
 * <p>
 * Acceptance criteria(условия принятия работы):
 * 1. В классе Calculator присутствуют перегруженные версии методов
 * add, sub, div, mul которые принимают на вход и возвращают числа типа double
 * 2. Тесты из CalculatorTest проходят
 * 3. Тесты из CalculatorForDoubleTest проходят
 * 4. В CalculatorForDoubleTest присутствуют тесты которые вы посчитаете
 * необходимыми реализовать для решения задачи
 * <p>
 * Подсказка:
 * Деление double на 0 возвращает не ошибку, а Infinity(бесконечность)
 * для проверки что вернулась бесконечность можно использовать конструкцию:
 * assertTrue(Double.isInfinite(result));
 */
public class CalculatorForDoubleTest {
}
