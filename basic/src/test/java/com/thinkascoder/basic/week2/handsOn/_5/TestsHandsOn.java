package com.thinkascoder.basic.week2.handsOn._5;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Исправте тесты что бы они проходили
 */
public class TestsHandsOn {
    public static int add(int a, int b) {
        return a + b;
    }

    @Test // Тестовые методы помечаются @Test
    public void simple_test() throws Exception {
        int expectedResult = 0; // 0?

        int sumResult = add(2, 2);

        assertEquals(expectedResult, sumResult);
    }

    @Test // Внутри теста нужно использовать определенную структуру - Given, When, Then
    public void test_structure() throws Exception {
        // Given - те данные, которые нам известны: в нашем случае - 2 параметра и возвращаемое значение
        int numberOne = 2;
        int numberTwo = -2;
        int expectedResult = 0; // 0?

        // When - то что мы тестируем(событие), в нашем случае - сложение положительного и отрицательного числа
        int sumResult = add(numberOne, numberTwo);

        // Then - утверждение, которым мы проверяем, что код работает корректно
        assertEquals(expectedResult, sumResult);
    }

    @Test // Есть разные утверждения
    public void different_asserts() throws Exception {
        // Почини их что бы понять чем они отличаются
        assertTrue(false);
        assertFalse(false);
        assertEquals(10, 20);
        assertNotEquals(10, 10);
    }

    /**
     * Для сравнения 2 значений типа double
     * нужно сравнивать их с точностью, например 0.01(до 2 знаков после запятой)
     */
    @Test
    public void double_equals_interesting_fact() throws Exception {
        assertEquals(0.5, 100500.5, 0.01); // исправь на истинное утверждение
    }

    @Test
    /*
    * Один тест тестирует один случай использования кода!
    * Из названия метода(тестового) должно быть понятно что тестируется
    * Вам понятно что проверяется в этом тесте?
    */
    public void add_should_sum_negative_numbers() throws Exception {
        // Given
        int expected = 0;

        // When
        int sumResult = add(-1, -2);

        // Then
        assertEquals(sumResult, expected);
    }

    @Test(expected = ArithmeticException.class) // так мы показываем что ждем ошибку в тесте
    public void arithmetic_exception_handling() throws Exception {
        int a = 100 / 10; // ожидается деление на 0
    }
}
