package com.thinkascoder.basic.week2.handsOn._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class ControlFlowHandsOn extends HandsOnRunner {
    @HandsOn("Код под if отрабатывает если условие истинно")
    public void simple_condition() throws Exception {
        int value = 0;
        if (true) {
            value = 100;
        }
        assertEquals(value, ___);
    }

    @HandsOn("В условии можно использовать логическое И")
    public void condition_with_AND_positive_clause() throws Exception {
        boolean hasMoney = true;
        boolean barIsOpen = true;
        int alcoholLevel = 0;

        if (hasMoney && barIsOpen) {
            alcoholLevel = 100500;
        }

        assertEquals(alcoholLevel, ___);
    }

    @HandsOn("Особенность логического И определяет, отработает код в блоке if или нет")
    public void condition_with_AND_negative_clause() throws Exception {
        boolean hasMoney = false;
        boolean barIsOpen = true;
        int alcoholLevel = 0;

        if (hasMoney && barIsOpen) {
            alcoholLevel = 100500;
        }

        assertEquals(alcoholLevel, ___);
    }


    @HandsOn("Установлен ли майкрософт оффис на этот ноутбук?")
    public void OR_condition_with_first_positive_clause() throws Exception {
        boolean isWindowsOS = true;
        boolean isMacOS = false;
        boolean installedMicrosoftOffice = false;

        if (isWindowsOS || isMacOS) {
            installedMicrosoftOffice = true;
        }

        assertEquals(installedMicrosoftOffice, ___);
    }

    @HandsOn("А на этот?")
    public void OR_condition_with_second_positive_clause() throws Exception {
        boolean isWindowsOS = false;
        boolean isMacOS = true;
        boolean installedMicrosoftOffice = false;

        if (isWindowsOS || isMacOS)
            installedMicrosoftOffice = true;

        assertEquals(installedMicrosoftOffice, ___);
    }

    @HandsOn("А на этот?")
    public void OR_condition_with_negative_clause() throws Exception {
        boolean isWindowsOS = false;
        boolean isMacOS = false; // походу Linux
        boolean installedMicrosoftOffice = false;

        if (isWindowsOS || isMacOS)
            installedMicrosoftOffice = true;

        assertEquals(installedMicrosoftOffice, ___);
    }

    @HandsOn("Блок else отрабатывает если условие в if ложно")
    public void else_handles_another_variants() throws Exception {
        int age = 18;
        boolean isEligibleForAlcohol = false;

        if (age > 21) {
            isEligibleForAlcohol = true;
        } else {
            isEligibleForAlcohol = false;
        }

        assertEquals(isEligibleForAlcohol, ___);
    }

    @HandsOn("С помощью конструкции if-else-if-else можно строить более сложные наборы условий")
    public void if_else_if_else_is_used_for_multiple_clauses() throws Exception {
        int percentOfCompletion = 50;
        String messageForUser;

        if (percentOfCompletion > 0 && percentOfCompletion <= 50)
            messageForUser = "Я начал установку";
        else if (percentOfCompletion > 50 && percentOfCompletion < 90)
            messageForUser = "Устанавливаю";
        else
            messageForUser = "Почти установил";

        assertEquals(messageForUser, ___);
    }

    @HandsOn("Какой код у программы?")
    public void sometimes_you_have_so_big_if_else() throws Exception {
        String userInput = "word";
        int programCode;

        if ("excel".equals(userInput)) {
            programCode = 1;
        } else if ("winamp".equals(userInput)) {
            programCode = 2;
        } else if ("chrome".equals(userInput)) {
            programCode = 3;
        } else if ("firefox".equals(userInput)) {
            programCode = 4;
        } else if ("explorer".equals(userInput)) {
            programCode = 5;
        } else if ("powerpoint".equals(userInput)) {
            programCode = 6;
        } else {
            programCode = 0;
        }

        assertEquals(programCode, ___);
    }

    @HandsOn("Switch позволяет проверять входной параметр на равенство разным вариантам")
    public void in_that_cases_its_convenient_to_use_switch_case() throws Exception {
        String userInput = "word";
        int programCode;

        switch (userInput) {
            case "excel":
                programCode = 1;
                break;
            case "winamp":
                programCode = 2;
                break;
            case "chrome":
                programCode = 3;
                break;
            case "firefox":
                programCode = 4;
                break;
            case "explorer":
                programCode = 5;
                break;
            case "powerpoint":
                programCode = 6;
                break;
            default:
                programCode = 0;
                break;
        }

        assertEquals(programCode, ___);
    }

    @HandsOn("А что будет если я не поставлю break? Зачем же break нужен? А почему можно без него?")
    public void if_you_dont_put_break_it_just_goes_down_until_first_break() throws Exception {
        int mark = 1;
        String examComment = "";

        switch (mark) {
            case 1:
            case 2:
                examComment = "Плохо";
                break;
            case 3:
                examComment = "Могло быть лучше";
                break;
            case 4:
            case 5:
                examComment = "Отлично!";
                break;
        }

        assertEquals(examComment, ___);
    }

    @HandsOn("А что если ни один вариант не совпадет?")
    public void default_works_if_no_case_matches() throws Exception {
        int mark = 100;
        String examComment = "";

        switch (mark) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                examComment = "Был на экзамене";
                break;
            default:
                examComment = "Похоже не был на экзамене";
        }

        assertEquals(examComment, ___);
    }

}
