package com.thinkascoder.basic.week2.homework;

import com.thinkascoder.basic.week2.homework._6.IsOddOrEven;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class IsOddOrEvenTest {
    @Test
    public void isEven_should_say_when_number_is_even() throws Exception {
        assertTrue(IsOddOrEven.isEven(2));
    }

    @Test
    public void isEven_should_say_when_number_is_not_even() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isEven_should_say_that_zero_is_even() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isOdd_should_say_when_number_is_odd() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isOdd_should_say_when_number_is_not_odd() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isOdd_should_say_that_zero_is_not_odd() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }
}
