package com.thinkascoder.basic.week2.homework;

import com.thinkascoder.basic.week2.homework._8.IntervalChecker;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Напишите тесты для проверки метода isInIntervalInclusive
 * <p>
 * 1. число в интервале
 * 2. число не в интервале
 * 3. число на границе в начале
 * 4. число на границе в конце
 */
public class IntervalCheckerTest {
    @Test
    public void isInIntervalExclusive_should_say_when_number_is_in_interval() throws Exception {
        assertTrue(IntervalChecker.isInIntervalExclusive(1, 10, 5));
    }

    @Test
    public void isInIntervalExclusive_should_say_when_number_is_not_in_interval() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isInIntervalExclusive_should_say_that_number_is_not_in_interval_when_number_in_the_beginning() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    @Test
    public void isInIntervalExclusive_should_say_that_number_is_not_in_interval_when_number_in_the_end() throws Exception {
        fail("Don't forget to replace this line with your implementation");
    }

    // Напишите тесты для isInIntervalInclusive ниже
}
