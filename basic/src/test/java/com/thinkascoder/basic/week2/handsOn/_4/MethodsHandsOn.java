package com.thinkascoder.basic.week2.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class MethodsHandsOn extends HandsOnRunner {
    public static int simpleMethod() {
        return 0;
    }

    public static String stringMethod() {
        return "";
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static void noReturnValue() {
        System.out.println("Я печатаю сообщение на экран");
    }

    @HandsOn("Методы могут иметь возвращаемое значение, определяемое через return")
    public void methods_has_return_value() throws Exception {
        assertEquals(simpleMethod(), ___);
    }

    @HandsOn("Тип возвращаемого значения указывается перед именем метода - String stringMethod()")
    public void method_has_return_type() throws Exception {
        assertEquals(stringMethod(), ___);
    }

    @HandsOn("Методы могут иметь входные параметры")
    public void methods_has_input_parameters_and_output_result() throws Exception {
        int multiplyResult = multiply(2, 3);
        assertEquals(multiplyResult, ___);
    }

    @HandsOn("Методы могут не возвращать значения")
    public void methods_can_have_no_output_value() throws Exception {
        fail("Видишь сообщение на экране? Тогда удали эту строку");
        noReturnValue();
    }
}
