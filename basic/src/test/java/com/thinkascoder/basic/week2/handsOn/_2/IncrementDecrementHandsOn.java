package com.thinkascoder.basic.week2.handsOn._2;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class IncrementDecrementHandsOn extends HandsOnRunner {
    @HandsOn("Как работает преинкремент?")
    public void prefix_increment_fist_increase_then_returns_value_of_variable() throws Exception {
        int i = 0;
        assertEquals(++i, ___);
        assertEquals(i, ___);
    }

    @HandsOn("Как работает постинкремент?")
    public void postfix_increment_returns_variable_value_then_increments() throws Exception {
        int i = 0;
        assertEquals(i++, ___);
        assertEquals(i, ___);
    }

    @HandsOn("Как работает предекремент?")
    public void prefix_decrement_first_decrease_then_returns_value_of_variable() throws Exception {
        int i = 10;
        assertEquals(--i, ___);
        assertEquals(i, ___);
    }

    @HandsOn("Как работает постдекремент?")
    public void postfix_decrement_returns_variable_value_then_decrements() throws Exception {
        int i = 10;
        assertEquals(i--, ___);
        assertEquals(i, ___);
    }

    @HandsOn("Примени ка на практике свои знания")
    public void task_for_increment_and_decrement() throws Exception {
        int a = 100;
        int b = 200;
        int c = 5 * ++a;
        int d = 5 * b++;

        assertEquals(a, ___);
        assertEquals(b, ___);
        assertEquals(c, ___);
        assertEquals(d, ___);
    }
}
