package com.thinkascoder.basic.week3.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class ArraysHandsOn extends HandsOnRunner {
    @HandsOn("Вы можете узнать размер массива с помощью свойства length")
    public void array_length_property() throws Exception {
        int[] arrayOne = new int[10];
        assertEquals(arrayOne.length, ___);
    }

    @HandsOn("Массивы могут быть любого типа")
    public void array_could_be_any_type() throws Exception {
        assertEquals(getType(new int[10]), ___); // подсказка: int[].class
        assertEquals(getType(new short[10]), ___);
        assertEquals(getType(new double[10]), ___);
        assertEquals(getType(new float[10]), ___);
        assertEquals(getType(new boolean[10]), ___);
        assertEquals(getType(new long[10]), ___);
        assertEquals(getType(new byte[10]), ___);
        assertEquals(getType(new String[10]), ___);
    }

    @HandsOn("Обращение к элементам массива по индексу начинается с 0")
    public void arrays_are_zero_based() throws Exception {
        int[] array = new int[10];
        array[0] = 100500;             // запись элемент в массива по индексу 0
        int readFromArray = array[0];  // чтение элемента массива по индексу 0
        assertEquals(readFromArray, ___);
    }

    @HandsOn("Какой последний индекс в массиве?")
    public void last_array_index() throws Exception {
        int[] array = new int[10];
        int lastIndex = array.length - 1;
        assertEquals(lastIndex, ___);
    }

    @HandsOn("С помощью цикла вы можете проходить все элементы массива")
    public void you_can_iterate_over_array_via_loops() throws Exception {
        int[] array = {1, 2, 3, 4, 5}; // сокращенная форма инициализации массива
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        assertEquals(sum, ___);
    }
}
