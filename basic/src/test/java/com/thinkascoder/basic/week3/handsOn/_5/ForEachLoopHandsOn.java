package com.thinkascoder.basic.week3.handsOn._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class ForEachLoopHandsOn extends HandsOnRunner {
    @HandsOn("С помощью for each гораздо проще проходить по всем элементам массива")
    public void you_can_iterate_over_array_via_for_each() throws Exception {
        int[] array = {1, 2, 3, 4, 5};
        int sum = 0;
        for (int element : array) {
            sum += element;
        }
        assertEquals(sum, ___);
    }

    @HandsOn("Вы можете пропускать элементы в for-each")
    public void you_can_skip_elements_via_for_each() throws Exception {
        int[] array = {1, 2, 3, 4, 5};
        int sumOfOdd = 0;
        for (int element : array) {
            if (element % 2 == 0)
                continue;
            else
                sumOfOdd += element;
        }
        assertEquals(sumOfOdd, ___);
    }

    @HandsOn("Так же отлично перебирать строки в for-each")
    public void its_very_convenient_to_iterate_over_strings_with_for_each() throws Exception {
        String[] names = new String[]{"Bob", "Jim", "Lauren", "Jeff"};
        int startsWithJ = 0;
        for (String name : names) {
            if (name.startsWith("J"))
                startsWithJ++;
        }
        assertEquals(startsWithJ, ___);
    }
}
