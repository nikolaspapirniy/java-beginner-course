package com.thinkascoder.basic.week3.handsOn._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class WhileLoopHandsOn extends HandsOnRunner {
    @HandsOn("Цикл while будет работать пока условие в скобках будет истинно")
    public void simple_while_loop() throws Exception {
        int counter = 0;
        while (counter < 5) {
            counter++;
        }
        assertEquals(counter, ___);
    }

    @HandsOn("Блок цикла while не отработает ни разу если условие ложно со старту ложно")
    public void while_not_executing_if_condition_is_false() throws Exception {
        int counter = 0;
        while (counter < 0) {
            counter++;
        }
        assertEquals(counter, ___);
    }

    @HandsOn("Условие цикла можно менять прямо внутри блока")
    public void while_condition_could_be_changed_from_inside_of_while_block() throws Exception {
        int counter = 10;
        boolean isRunning = true;

        while (isRunning) {
            if (counter % 13 == 0)
                isRunning = false;
            else
                counter++;
        }
        assertEquals(counter, ___);
    }

    @HandsOn("break позволяет останавливать выполнение цикла")
    public void break_simply_breaks_loop_execution() throws Exception {
        int counter = 0;
        while (true) { // это бесконечный цикл
            break;
        }
        assertEquals(counter, ___);
    }

    @HandsOn("break полезен в бесконечных циклах")
    public void break_useful_for_infinite_loops() throws Exception {
        int counter = 1;
        while (true) { // это бесконечный цикл
            if (counter % 10 == 0)
                break;
            else
                counter++;
        }
        assertEquals(counter, ___);
    }

    @HandsOn("continue используется что бы пропустить итерацию")
    public void continue_is_used_to_skip_iteration() throws Exception {
        int sumOfOdd = 0;
        int i = 0;

        while (i < 10) {
            i++;
            if (i % 2 == 0) {
                continue;
            }
            sumOfOdd += i;
        }
        assertEquals(sumOfOdd, ___);
    }
}
