package com.thinkascoder.basic.week3.handsOn._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class TwoDimensionalArraysHandsOn extends HandsOnRunner {
    @HandsOn("Массивы могут быть двумерные. По сути это массив-массивов")
    public void two_dimensional_arrays_read_write() throws Exception {
        int[][] arrays = new int[2][2];
        arrays[0][0] = 100500;             // запись элемент в массива по индексу 0 0
        int readFromArray = arrays[0][0];  // чтение элемента массива по индексу 0 0
        assertEquals(readFromArray, ___);
    }

    @HandsOn("Итерироваться по двумерному массиву можно через вложенный(двойной) цикл")
    public void iterate_over_two_dimensional_arrays() throws Exception {
        // двумерный массив можно и так инициализировать
        int[][] arrays = {
                {1, 2},
                {3, 4}
        };

        int sum = 0;
        for (int i = 0; i < arrays.length; i++) {
            // обратите внимание на (j < arrays[i].length). Все потому что каждый массив, содержит массив
            for (int j = 0; j < arrays[i].length; j++) {
                sum += arrays[i][j]; // заметили [i][j] ?
            }
        }
        assertEquals(sum, ___);
    }

    @HandsOn("Вы можете итерироваться по двумерному массиву через for-each")
    public void iterate_over_two_dimensional_arrays_with_for_each() throws Exception {
        int[][] arrays = {
                {1, 2},
                {3, 4}
        };

        int sum = 0;
        // Заметили слева int[] array?
        for (int[] array : arrays) {
            for (int value : array) {
                sum += value;
            }
        }
        assertEquals(sum, ___);
    }

    @HandsOn("Двумерные массивы могут иметь внутри массивы разного размера(но это явно не очень хорошо)")
    public void two_dimensional_arrays_with_different_size() throws Exception {
        int[][] arrays = {
                {1},
                {2, 3},
                {3, 4, 5},
                {6}
        };

        int sum = 0;
        for (int[] array : arrays) {
            for (int value : array) {
                sum += value;
            }
        }
        assertEquals(sum, ___);
    }
}
