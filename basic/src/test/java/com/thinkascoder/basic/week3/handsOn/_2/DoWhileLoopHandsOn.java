package com.thinkascoder.basic.week3.handsOn._2;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class DoWhileLoopHandsOn extends HandsOnRunner {
    @HandsOn("do-while, работает как while, только проверка условия происходит после 1 итерации")
    public void simple_do_while_loop() throws Exception {
        int result = 100;
        do {
            result--;
        } while (result > 0);
        assertEquals(result, ___);
    }

    @HandsOn("do-while отработает хотя бы 1 раз")
    public void do_while_works_at_least_one_time() throws Exception {
        int result = 0;
        do {
            result++;
        } while (false);
        assertEquals(result, ___);
    }

    @HandsOn("break работает с do-while так же как и с while")
    public void break_works_with_do_while_the_same_as_with_while() throws Exception {
        int result = 0;
        do {
            if (result == 20)
                break;
            else
                result++;
        } while (result < 100);
        assertEquals(result, ___);
    }

    @HandsOn("continue работает с do-while так же как и с while")
    public void continue_works_with_do_while_the_same_as_with_while() throws Exception {
        int sumOfEven = 0;
        int i = 0;
        do {
            i++;
            if (i % 2 != 0)
                continue;

            sumOfEven += i;
        } while (i < 9); // почему что бы посчитать сумму четных до 10, нужно условие i < 9 ?
        assertEquals(sumOfEven, ___);
    }

}
