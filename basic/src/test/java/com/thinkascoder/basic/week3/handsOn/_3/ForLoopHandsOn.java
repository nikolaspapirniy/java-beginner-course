package com.thinkascoder.basic.week3.handsOn._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class ForLoopHandsOn extends HandsOnRunner {
    @HandsOn("for - удобрый цикл со встроенным блоком инициализации, условием выхода, действием после итерации")
    public void simple_for_loop() throws Exception {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += i;
        }
        assertEquals(sum, ___);
    }

    @HandsOn("for - всего навсего упрощенная и компактная форма цикла while")
    public void for_is_simplify_while() throws Exception {
        // while версия
        int sumOfI = 0;
        int i = 0;
        while (i < 10) {
            sumOfI += i;
            i++;
        }
        assertEquals(sumOfI, ___);

        // for версия
        int sumOfJ = 0;
        for (int j = 0; j < 10; j++) {
            sumOfJ += j;
        }
        assertEquals(sumOfJ, ___);
    }

    @HandsOn("for - может быть выражен и через do-while")
    public void for_could_be_expressed_as_do_while() throws Exception {
        // do-while версия
        int sumOfI = 0;
        int i = 0;
        do {
            if (i > 10) // дополнительная проверка, потому как блок do вызывается до проверки
                break;

            sumOfI += i;
            i++;
        } while (i < 10);
        assertEquals(sumOfI, ___);

        // for версия
        int sumOfJ = 0;
        for (int j = 0; j < 10; j++) {
            sumOfJ += j;
        }
        assertEquals(sumOfJ, ___);
    }

    @HandsOn("в блоке иницилизации можно обьявлять и присваивать значения переменным(разным)")
    public void for_init_block() throws Exception {
        int sum = 0;
        for (int i = 1, j = 10; i < 10; i++) {
            sum += j;
        }
        assertEquals(sum, ___);
    }

    @HandsOn("условие может быть любым придекатом")
    public void condition_block_with_predicate() throws Exception {
        int sum = 0;
        boolean condition = true;
        for (int i = 0; i < 10 && condition; i++) {
            sum += i;
        }
        assertEquals(sum, ___);
    }

    @HandsOn("действие после итерации может быть любым оператором")
    public void action_after_iteration_could_be_any_operator() throws Exception {
        int sumEachThird = 0;
        for (int i = 0; i < 10; i += 3) {
            sumEachThird += i;
        }
        assertEquals(sumEachThird, ___);
    }
}
