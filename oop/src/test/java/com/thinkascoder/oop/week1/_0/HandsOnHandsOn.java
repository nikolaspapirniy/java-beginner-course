package com.thinkascoder.oop.week1._0;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class HandsOnHandsOn extends HandsOnRunner {
    @HandsOn("Через запятую должны стоять эквивалентные значения (например 2 - 2, 0)")
    public void you_need_to_replace____with_your_answer() throws Exception {
        assertEquals(2 - 2, ___);
    }

    @HandsOn("HandsOn - маленькие примеры кода, они показывают, как писать код")
    public void what_handsOn_can() throws Exception {
        assertEquals(2 + 2, ___);
    }

    @HandsOn("Нужно написать ответ, глядя на левую сторону(не запуская код, если это возможно)")
    public void think_before_run() throws Exception {
        assertEquals(2 + 2, ___);
    }

    @HandsOn("Если совсем сложно и не знаешь ответа, то просто запусти код")
    public void if_dont_know() throws Exception {
        assertEquals(Integer.MAX_VALUE, ___);
        // Запусти код и узнаешь ответ: System.out.println(Integer.MAX_VALUE);
    }

    @HandsOn("Строку с fail нужно просто удалить. ")
    public void just_remove_fail_line_to_pass() throws Exception {
        fail("Это заглушка");
    }
}
