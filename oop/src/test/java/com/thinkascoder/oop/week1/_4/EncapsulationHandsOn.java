package com.thinkascoder.oop.week1._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class EncapsulationHandsOn extends HandsOnRunner {
    @HandsOn("Инкапсуляция - основной принцип ООП. " +
            "Он говорит о том, что, когда люди садятся за руль и хотят доехать до работы, " +
            "возможно, их не интерисует, как устроена сама машина")
    public void what_is_encapsulation() {
        class Car {
            // мы помечаем private поля, которые пользователю нашего класса не стоит знать
            private double engineTubeTemperature; // возможно, пользователю не важна температура трубки в двигателе
            private double valvePressure; // так же ему все равно на давление в клапане
            // ......
            // другие переменные, которые не важны пользователю

            // А вот методы управления машиной ему очень важны
            public void go() {
                System.out.println("Поехали!");
            }

            public void stop() {
                System.out.println("Стопэ");
            }

            public void turnRight() {
                System.out.println("Я повернул вправо");
            }

            public void turnLeft() {
                System.out.println("Я повернул влево");
            }
        }

        Car car = new Car();
        car.go();
        car.turnLeft();
        car.turnRight();
        car.stop();

        // А этот код не скомпилируется потому, как мы явно указали, что эти поля будут скрытыми от чужих глаз

        // попробуй их раскомментировать
        // double a = car.engineTubeTemperature;
        // double b = car.valvePressure;

        fail("Ааааа, я понял! Private переменные нельзя вызывать за пределами класса, а public можно!");
    }

    @HandsOn("Инкапсуляцию стоит использовать, когда хочешь спрятать делали реализации от пользователя")
    public void when_to_use_encapsulation() {
        class SalaryCalculator {
            private double taxPercent = 5;
            private double rate = 0.21;

            public double getMySalary(double money) {
                return money - (money / 100 * taxPercent) - money * rate;
            }
        }
        SalaryCalculator salaryCalculator = new SalaryCalculator();
        // человеку не важны расчеты, он хочет зарплату!
        System.out.println(salaryCalculator.getMySalary(2500));
        assertEquals(salaryCalculator.getMySalary(2500), ___);
    }

    @HandsOn("В java принято обращаться к полям класса через методы - getters и setters")
    public void about_getters_and_setters() {
        class Person {
            private String name;
            private int age;

            public Person(String name, int age) {
                this.name = name;
                this.age = age;
            }

            // getters - дают нам возможность контролировать выдачу данных
            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getAge() {
                return age;
            }

            // setters - дают нам возможность контролировать присваивание значений
            public void setAge(int age) {
                if (age > 0)
                    this.age = age;
            }
        }
        Person person1 = new Person("Ivan", 21);
        person1.setAge(35);

        Person person2 = new Person("Katya", 31);
        person2.setName("Serega");

        assertEquals(person1.getName(), ___);
        assertEquals(person1.getAge(), ___);

        assertEquals(person2.getName(), ___);
        assertEquals(person2.getAge(), ___);
    }
}
