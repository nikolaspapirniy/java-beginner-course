package com.thinkascoder.oop.week1._8;

import org.junit.Test;

import static com.thinkascoder.system.validations.ConstructorValidations.assertHasConstructor;
import static com.thinkascoder.system.validations.ConstructorValidations.assertHasDefaultConstructor;
import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertPrivateField;
import static com.thinkascoder.system.validations.MethodValidations.*;

/**
 * Запусти, когда реализуешь класс MyString
 */
public class MyStringSignatureTest {
    public static final String STRING_FIELD_NAME = "string";

    public static final String SUBSTRING_METHOD_NAME = "substring";
    public static final String INDEXOF_METHOD_NAME = "indexOf";
    public static final String COUNT_METHOD_NAME = "count";
    public static final String CHAR_AT_METHOD_NAME = "charAt";
    public static final String CONCAT_METHOD_NAME = "concat";
    public static final String SPLIT_METHOD_NAME = "split";
    public static final String SIZE_METHOD_NAME = "size";
    public static final String TO_UPPER_CASE_METHOD_NAME = "toUpperCase";
    public static final String TO_LOWER_CASE_METHOD_NAME = "toLowerCase";

    @Test
    public void validate_string_field() throws Exception {
        assertHasField(MyString.class, STRING_FIELD_NAME, char[].class);
    }

    @Test
    public void validate_string_field_is_private() throws Exception {
        assertPrivateField(MyString.class, STRING_FIELD_NAME);
    }

    @Test
    public void validate_empty_constructor() throws Exception {
        assertHasDefaultConstructor(MyString.class);
    }

    @Test
    public void validate_constructor_with_param() throws Exception {
        assertHasConstructor(MyString.class, char[].class);
    }

    @Test
    public void validate_substring_method_signature() throws Exception {
        assertHasMethod(MyString.class, SUBSTRING_METHOD_NAME);
        assertMethodParams(MyString.class, SUBSTRING_METHOD_NAME, int.class, int.class);
        assertMethodReturn(MyString.class, SUBSTRING_METHOD_NAME, MyString.class);
    }

    @Test
    public void validate_indexOf_method_signature() throws Exception {
        assertHasMethod(MyString.class, INDEXOF_METHOD_NAME);
        assertMethodParams(MyString.class, INDEXOF_METHOD_NAME, MyString.class);
        assertMethodReturn(MyString.class, INDEXOF_METHOD_NAME, int.class);
    }

    @Test
    public void validate_count_method_signature() throws Exception {
        assertHasMethod(MyString.class, COUNT_METHOD_NAME);
        assertMethodParams(MyString.class, COUNT_METHOD_NAME, MyString.class);
        assertMethodReturn(MyString.class, COUNT_METHOD_NAME, int.class);
    }

    @Test
    public void validate_charAt_method_signature() throws Exception {
        assertHasMethod(MyString.class, CHAR_AT_METHOD_NAME);
        assertMethodParams(MyString.class, CHAR_AT_METHOD_NAME, int.class);
        assertMethodReturn(MyString.class, CHAR_AT_METHOD_NAME, char.class);
    }

    @Test
    public void testName() throws Exception {
        char[] first = {'a', 'b', 'c'};
        char[] second = {'1', '2'};

        char[] r = new char[first.length + second.length];
        System.arraycopy(first, 0, r, 0, first.length);
        System.arraycopy(second, 0, r, 0, first.length);

    }

    @Test
    public void validate_concat_method_signature() throws Exception {
        assertHasMethod(MyString.class, CONCAT_METHOD_NAME);
        assertMethodParams(MyString.class, CONCAT_METHOD_NAME, MyString.class);
        assertMethodReturn(MyString.class, CONCAT_METHOD_NAME, MyString.class);
    }


    @Test
    public void validate_size_method_signature() throws Exception {
        assertHasMethod(MyString.class, SIZE_METHOD_NAME);
        assertMethodHasNoParams(MyString.class, SIZE_METHOD_NAME);
        assertMethodReturn(MyString.class, SIZE_METHOD_NAME, int.class);
    }

    @Test
    public void validate_toUpperCase_method_signature() throws Exception {
        assertHasMethod(MyString.class, TO_UPPER_CASE_METHOD_NAME);
        assertMethodHasNoParams(MyString.class, TO_UPPER_CASE_METHOD_NAME);
        assertMethodReturn(MyString.class, TO_UPPER_CASE_METHOD_NAME, MyString.class);
    }

    @Test
    public void validate_toLowerCase_method_signature() throws Exception {
        assertHasMethod(MyString.class, TO_LOWER_CASE_METHOD_NAME);
        assertMethodHasNoParams(MyString.class, TO_LOWER_CASE_METHOD_NAME);
        assertMethodReturn(MyString.class, TO_LOWER_CASE_METHOD_NAME, MyString.class);
    }

    @Test
    public void validate_split_method_signature() throws Exception {
        assertHasMethod(MyString.class, SPLIT_METHOD_NAME);
        assertMethodParams(MyString.class, SPLIT_METHOD_NAME, MyString.class);
        assertMethodReturn(MyString.class, SPLIT_METHOD_NAME, MyString[].class);
    }
}
