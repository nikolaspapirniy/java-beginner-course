package com.thinkascoder.oop.week1._7;

import com.thinkascoder.oop.week1._7.Fraction;
import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import static com.thinkascoder.system.validations.ConstructorValidations.assertHasConstructor;
import static com.thinkascoder.system.validations.ConstructorValidations.assertHasNoDefaultConstructor;
import static com.thinkascoder.system.validations.FieldValidations.assertFinalField;
import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertPrivateField;
import static com.thinkascoder.system.validations.MethodValidations.*;

// Эти тесты помогут тебе понять, правильно ли ты создал методы и поля
// Запусти их, когда реализуешь класс Fraction
// (здесь писать тесты не нужно)
public class FractionSignatureTest extends Validations {
    public static final String NUMERATOR_FIELD_NAME = "numerator";
    public static final String DENOMINATOR_FIELD_NAME = "denominator";

    public static final String ADD_METHOD_NAME = "add";
    public static final String SUB_METHOD_NAME = "sub";
    public static final String MUL_METHOD_NAME = "mul";
    public static final String DIV_METHOD_NAME = "div";


    @Test
    public void validate_fraction_fields() throws Exception {
        assertHasField(Fraction.class, NUMERATOR_FIELD_NAME, int.class);
        assertFinalField(Fraction.class, NUMERATOR_FIELD_NAME);
        assertHasField(Fraction.class, DENOMINATOR_FIELD_NAME, int.class);
        assertFinalField(Fraction.class, DENOMINATOR_FIELD_NAME);
    }

    @Test
    public void validate_fraction_fields_modifiers() throws Exception {
        assertPrivateField(Fraction.class, NUMERATOR_FIELD_NAME);
        assertPrivateField(Fraction.class, DENOMINATOR_FIELD_NAME);
    }

    @Test
    public void validate_no_default_constructor() throws Exception {
        assertHasNoDefaultConstructor(Fraction.class);
    }

    @Test
    public void validate_constructor_with_int_params() throws Exception {
        assertHasConstructor(Fraction.class, int.class, int.class);
    }

    @Test
    public void validate_constructor_with_fraction_param() throws Exception {
        assertHasConstructor(Fraction.class, Fraction.class);
    }

    @Test
    public void validate_add_method() throws Exception {
        assertHasMethod(Fraction.class, ADD_METHOD_NAME);
        assertMethodParams(Fraction.class, ADD_METHOD_NAME, Fraction.class);
        assertMethodReturn(Fraction.class, ADD_METHOD_NAME, Fraction.class);
    }


    @Test
    public void validate_sub_method() throws Exception {
        assertHasMethod(Fraction.class, SUB_METHOD_NAME);
        assertMethodParams(Fraction.class, SUB_METHOD_NAME, Fraction.class);
        assertMethodReturn(Fraction.class, SUB_METHOD_NAME, Fraction.class);
    }

    @Test
    public void validate_mul_method() throws Exception {
        assertHasMethod(Fraction.class, MUL_METHOD_NAME);
        assertMethodParams(Fraction.class, MUL_METHOD_NAME, Fraction.class);
        assertMethodReturn(Fraction.class, MUL_METHOD_NAME, Fraction.class);
    }

    @Test
    public void validate_div_method() throws Exception {
        assertHasMethod(Fraction.class, DIV_METHOD_NAME);
        assertMethodParams(Fraction.class, DIV_METHOD_NAME, Fraction.class);
        assertMethodReturn(Fraction.class, DIV_METHOD_NAME, Fraction.class);
    }
}
