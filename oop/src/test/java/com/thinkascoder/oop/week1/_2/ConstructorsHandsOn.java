package com.thinkascoder.oop.week1._2;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class ConstructorsHandsOn extends HandsOnRunner {
    @HandsOn("Порой создавать объект и инициализировать поля бывает удобно одновременно. Это делается с помощью конструктора")
    public void constructor_is_very_convenient() throws Exception {
        class Fraction {
            int numerator;
            int denominator;

            // Конструктор - метод, который вызывается во время создании обьекта
            // он полезен для инициализации значений

            // Это конструктор по умолнчанию, он создается автоматически
            public Fraction() {
            }

            // Это перегруженный конструктор(как перегруженный метод), который инициализирует дробь
            // значениями числитель и знаменатель
            public Fraction(int numerator, int denominator) {
                // this - ссылка на обьект, в котором мы сейчас находимся
                // как ты можешь заметить, поле и параметр называются одинаково(так принято в java)
                // что бы установить значение именно поля, мы к нему обращаемся через this
                this.numerator = numerator;
                this.denominator = denominator;
            }
        }

        Fraction fractionWithDefaultConstructor = new Fraction();
        assertEquals(fractionWithDefaultConstructor.numerator, ___);
        assertEquals(fractionWithDefaultConstructor.denominator, ___);

        Fraction fractionCreatedByCustomConstructor = new Fraction(1, 2);
        assertEquals(fractionCreatedByCustomConstructor.numerator, ___);
        assertEquals(fractionCreatedByCustomConstructor.denominator, ___);
    }

    @HandsOn("Если ты не передаешь параметры в конструктор, то вызывается конструктор по умолчанию")
    public void default_constructor() throws Exception {
        class ClassWithDefaultConstructor {
            int age;

            ClassWithDefaultConstructor() {
                age = 21;
            }
        }

        ClassWithDefaultConstructor classWithDefaultConstructor = new ClassWithDefaultConstructor();
        assertEquals(classWithDefaultConstructor.age, ___);
    }


    @HandsOn("Так же можно инициализировать поле прямо в определении. " +
            "Любой созданный обьект присвоит значение полю во время конструирования")
    public void inline_initialisation() throws Exception {
        class Invitation {
            String from = "Nick";
            String to;

            public Invitation(String to) {
                this.to = to;
            }
        }
        Invitation annInvite = new Invitation("Ann");
        assertEquals(annInvite.from, ___);
        assertEquals(annInvite.to, ___);

        Invitation steveInvite = new Invitation("Steve");
        assertEquals(steveInvite.from, ___);
        assertEquals(steveInvite.to, ___);
    }

    @HandsOn("this - ссылка на обьект, в котором находимся")
    public void this_reference() {
        class ThisExample {
            int value = 50;

            int getByThis(int value) {
                return this.value;
            }

            int getWithoutThis(int value) {
                return value;
            }
        }

        ThisExample thisExample = new ThisExample();

        assertEquals(thisExample.getByThis(21), ___);
        assertEquals(thisExample.getWithoutThis(21), ___);
    }

    @HandsOn("Из одного конструктора можно вызвать другой, это удобно что бы не дублировать код")
    public void class_constructor_from_another_constructor() {
        class Student {
            String name;
            String group;
            String favouriteSubject;

            public Student(String name, String group) {
                this.name = name;
                this.group = group;
            }

            public Student(String name, String group, String favouriteSubject) {
                // первой строкой в конструкторе может быть вызов другого конструктора.
                // Эта конструкция нужна что бы не дублировать строки
                // this.name = name;
                // this.group = group;

                this(name, group);

                this.favouriteSubject = favouriteSubject;
            }
        }

        Student student1 = new Student("Иван", "МТ-102");
        Student student2 = new Student("Иван", "МТ-102", "Программирование");

        assertEquals(student1.name, ___);
        assertEquals(student2.name, ___);

        assertEquals(student1.favouriteSubject, ___);
        assertEquals(student2.favouriteSubject, ___);
    }
}
