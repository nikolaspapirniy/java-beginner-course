package com.thinkascoder.oop.week1._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class ClassesHandsOn extends HandsOnRunner {
    @HandsOn("Класс-это просто тип данных, созданный пользователем, он представляет комплексную модель из реального мира")
    public void simple_class_example() throws Exception {
        // Классы можно обьявлять внутри методов, внутри классов, в отдельном файле или в файле где уже есть класс
        class Point {
            int x;
            int y;
        }

        // Точка в двумерном пространстве это обьект с 2 координатами х и у
        fail("Удали эту сроку, когда разберешься в этом handsOn примере");
    }

    @HandsOn("Класс - это формочка для печенья, он хранит поля - свойства печенья")
    public void fields_in_class() throws Exception {
        class Person {
            String name;
            int age;
        }

        // x и у - поля(свойства) класса. Они могут быть любого типа данных
        fail("Удали эту сроку, когда разберешься в этом handsOn примере");
    }

    @HandsOn("Из формочки для печения можно лепить печенье, так и из класса можно создавать конкретные обьекты")
    public void objects_creation_example() throws Exception {
        class Cookie {
            String structure;
            boolean hasRaisins;
        }

        // Создадим ссылку типа Cookie, которая еще не инициализированна
        // Ссылка, это как веб-ссылка на google.com - это не сайт, а просто адрес по которому можно добраться до сайта
        // так же ссылка это координаты gps(N 50 45 16.2, E 6 1 16), она просто указывает где находится обьект
        // пока что ссылка указывает в null. Null - ссылка в никуда(вернее сказать null показывает что она не ссылается на обьект)
        Cookie chocolateCookieWithRaisins = null;

        // оператор new - создает новый обьект и возвращает ссылку на созданный обьект
        chocolateCookieWithRaisins = new Cookie();

        // присваивать значения полям так же как и переменным, только нужно указать ссылку на конкретный обьект
        chocolateCookieWithRaisins.structure = "chocolate";
        chocolateCookieWithRaisins.hasRaisins = true;

        assertEquals(chocolateCookieWithRaisins.structure, ___);
        assertEquals(chocolateCookieWithRaisins.hasRaisins, ___);
    }

    @HandsOn("В чем разница между классом и обьектом? ")
    public void difference_between_class_and_object() throws Exception {
        // Отличный пример класса - запись в телефонной книге
        // Класс помогает виртуальной машине java создавать конкретные обьекты
        class Record {
            String name;
            String phoneNumber;
        }

        Record ivanRecord = new Record();
        ivanRecord.name = "Jim";
        ivanRecord.phoneNumber = "093-65-64-212";

        Record mishaRecord = new Record();
        mishaRecord.name = "Misha";
        mishaRecord.phoneNumber = "064-65-61-124";

        assertEquals(ivanRecord.name, ___);
        assertEquals(mishaRecord.name, ___);
    }

    @HandsOn("Ссылки указывают на обьект, но он может быть изменен через другую ссылку")
    public void change_object_by_another_reference() {
        class Person {
            String name;
            int age;
        }

        Person person = new Person();
        person.name = "Раиса Джоновна";
        person.age = 32;

        Person reference = person; // теперь reference имеет ссылку на тот же обьект что и person

        // Мы меняем сам обьект, а это значит, что другие ссылки увидят обновленный обьект
        reference.name = "Поменяли";

        // Это можно представить, что как будто у двух человек на листике записан адрес квартиры.
        // Если в квартире по этому адресу сделают ремонт, то когда они придут они увидят, что квартира изменилась,
        // а ссылка(адрес) остался прежним

        assertEquals(person.name, ___);
        assertEquals(reference.name, ___);
    }

    @HandsOn("Классы могут иметь методы")
    public void classes_can_have_method() throws Exception {
        class Rectangle {
            int horizontalSide;
            int verticalSide;

            // Обьявлять методы ты уже умеешь
            int square() {
                // Обрати внимание, что этот метод имеет доступ к полям класса
                // то есть, если вызвать этот метод на разных обьектах(с разными полями), результат будет разный
                return horizontalSide * verticalSide;
            }
        }

        Rectangle rectangle1 = new Rectangle();
        rectangle1.horizontalSide = 2;
        rectangle1.verticalSide = 4;
        assertEquals(rectangle1.square(), ___);

        Rectangle rectangle2 = new Rectangle();
        rectangle2.horizontalSide = 10;
        rectangle2.verticalSide = 3;
        assertEquals(rectangle2.square(), ___);
    }

    @HandsOn("Методы в классе могут быть перегруженными")
    public void methods_in_classes_could_be_overloaded() throws Exception {
        class ClassWithOverloadedMethod {
            String method(String a) {
                return "String";
            }

            String method(int a) {
                return "Int";
            }

            String method(double a) {
                return "Double";
            }
        }
        ClassWithOverloadedMethod object = new ClassWithOverloadedMethod();
        assertEquals(object.method(1), ___);
        assertEquals(object.method(2.5), ___);
        assertEquals(object.method("100500"), ___);
    }

    @HandsOn("Методы могут вести себя по разному в зависимости от поля обьекта")
    public void method_behave_related_to_field() throws Exception {
        class Person {
            String position;

            String sayHello() {
                if ("Президент".equals(position))
                    return "кто его сюда пустил?";
                else if ("Программист".equals(position))
                    return "Здорова!";
                else
                    return "Добрый день";
            }
        }
        Person president = new Person();
        president.position = "Президент";

        Person coder = new Person();
        coder.position = "Программист";

        Person barber = new Person();
        barber.position = "Парикмахер";

        // Разные люди здороваются по разному
        assertEquals(president.sayHello(), ___);
        assertEquals(coder.sayHello(), ___);
        assertEquals(barber.sayHello(), ___);
    }

    @HandsOn("Как ты думаешь какие значения по умолчанию у полей класса?")
    public void default_values_in_class() throws Exception {
        class House {
            int floors;
            double averageSquareMetersPerFlat;
            long countOfBricks;
            boolean hasConcierge;
            char typeOfHouse;
            String builderCompany;
            int flatNumbers[];
        }

        House house = new House();
        assertEquals(house.floors, ___);
        assertEquals(house.averageSquareMetersPerFlat, ___);
        assertEquals(house.countOfBricks, ___);
        assertEquals(house.hasConcierge, ___);
        assertEquals(house.typeOfHouse, ___);
        assertEquals(house.builderCompany, ___); // подсказка: как показывается ссылка в никуда?
        assertEquals(house.flatNumbers, ___); // подсказка: массив это тоже обьект
    }
}
