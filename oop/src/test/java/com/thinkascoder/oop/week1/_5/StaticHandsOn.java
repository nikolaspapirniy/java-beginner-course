package com.thinkascoder.oop.week1._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class StaticHandsOn extends HandsOnRunner {
    static class MyMathClass {
        // этот метод не зависит от полей класса, а зависит только от входных параметров
        public static int sum(int a, int b) {
            return a + b;
        }

        public static int sub(int a, int b) {
            return a - b;
        }
    }

    @HandsOn("Некоторые значения общие для всех обьектов и не зависят от полей класса")
    public void static_example() {
        // так гораздо удобнее
        MyMathClass.sum(2, 3);
        MyMathClass.sub(2, 3);

        // чем создавать каждый раз обьект
        new MyMathClass().sum(2, 3);
        new MyMathClass().sub(2, 3);

        fail("Аааа поняла! Ведь здесь создавать обьект то и не надо");
    }

    static class UsersCounter {
        public int maxSiteVisits = 10;
        public static int countOfUsers = 0;


        static void visitWebSite() {
            countOfUsers++;
            // А здесь будет ошибка компиляции
            // System.out.println(maxSiteVisits);
            // Из статического метода можно обращаться только к статическим полям и методам.
        }
    }

    @HandsOn("static поля и методы единственны для всех обьектов класса")
    public void static_is_single_for_all_instances() {
        UsersCounter usersCounter1 = new UsersCounter();
        UsersCounter usersCounter2 = new UsersCounter();

        usersCounter1.visitWebSite();
        usersCounter1.maxSiteVisits = 100500;

        usersCounter2.visitWebSite();
        usersCounter2.maxSiteVisits = 200500;

        assertEquals(usersCounter1.maxSiteVisits, ___);
        assertEquals(usersCounter1.countOfUsers, ___);


        assertEquals(usersCounter2.maxSiteVisits, ___);
        assertEquals(usersCounter2.countOfUsers, ___);
        //смысл этого задания в том что бы понять что статическое поле, которе меняется из разных обьектов, все равно одно на всех
    }

    static class MySuperDuperMath {
        // public static final поля называются константами
        // они единственные в программе и не могут измениться
        // имя константы пишется большими буквами через подчеркивание(например MAX_COUNT_OF_USERS)
        public static final double PI = 3.14;

        public static double squareOfCircle(double r) {
            return MySuperDuperMath.PI * MySuperDuperMath.pow(r);
        }

        public static double pow(double value) {
            return value * value;
        }
    }

    @HandsOn("К static полям и методам нужно обращаться по имени класса, а не по ссылке на обьект")
    public void static_methods_and_fields_reference() {
        assertEquals(MySuperDuperMath.PI, ___);
        assertEquals(MySuperDuperMath.squareOfCircle(2.5), ___);
    }
}
