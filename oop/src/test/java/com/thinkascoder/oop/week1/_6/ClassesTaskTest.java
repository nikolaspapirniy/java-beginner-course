package com.thinkascoder.oop.week1._6;

import com.thinkascoder.system.validations.Validations;
import org.junit.Test;

import java.util.Date;

import static com.thinkascoder.system.validations.FieldValidations.assertHasField;

// Здесь тебе тесты писать не надо, все уже написано
public class ClassesTaskTest extends Validations {
    @Test
    public void validate_book_class() throws Exception {
        assertHasField(Book.class, "name", String.class);
        assertHasField(Book.class, "year", Date.class);
        assertHasField(Book.class, "pages", int.class);
        assertHasField(Book.class, "authors", Author[].class);
    }

    @Test
    public void validate_author_class() throws Exception {
        assertHasField(Author.class, "books", Book[].class);
        assertHasField(Author.class, "name", String.class);
        assertHasField(Author.class, "age", int.class);
        assertHasField(Author.class, "bio", String.class);
    }

    @Test
    public void validate_publisher_class() throws Exception {
        assertHasField(Publisher.class, "books", Book[].class);
        assertHasField(Publisher.class, "name", String.class);
        assertHasField(Publisher.class, "established", int.class);
    }
}
