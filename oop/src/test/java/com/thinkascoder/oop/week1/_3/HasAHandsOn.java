package com.thinkascoder.oop.week1._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

public class HasAHandsOn extends HandsOnRunner {
    @HandsOn("Обьект может состоять из ссылок на другие обьекты. Как, например, обьект телефон содержит обьект экран")
    public void objects_composition_example() throws Exception {
        class Screen {
            double sizeInInches;

            Screen(double sizeInInches) {
                this.sizeInInches = sizeInInches;
            }
        }

        class Phone {
            String brand;
            String model;

            Screen screen;
        }

        Phone iphone = new Phone();
        iphone.brand = "Apple";
        iphone.model = "6";
        iphone.screen = new Screen(4.7); // мы создаем обьект Screen и присваиваем ссылку на него сразу в screen

        assertEquals(iphone.screen.sizeInInches, ___);
    }

    @HandsOn("Обьект может ссылаться на ссылку того же типа, что и сам. " +
            "Например, человек в очереди видит перед собой другого человека в очереди")
    public void reference_to_same_type() throws Exception {

        class PersonInQueue {
            PersonInQueue nextPerson;
            String name;

            public PersonInQueue(String name) {
                this.name = name;
            }
        }

        /**
         * Подсказка:
         * Очередь за хлебом
         * --------------
         * Олег
         * Роман
         * Татьяна
         */

        PersonInQueue firstPerson = new PersonInQueue("Олег");

        PersonInQueue secondPerson = new PersonInQueue("Роман");
        secondPerson.nextPerson = firstPerson;

        PersonInQueue thirdPerson = new PersonInQueue("Татьяна");
        thirdPerson.nextPerson = secondPerson;

        assertEquals(thirdPerson.nextPerson.name, ___);
        assertEquals(thirdPerson.nextPerson.nextPerson.name, ___);
        assertEquals(thirdPerson.nextPerson.nextPerson.nextPerson, ___);
    }
}
