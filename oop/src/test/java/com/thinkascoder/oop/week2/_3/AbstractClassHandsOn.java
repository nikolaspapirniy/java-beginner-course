package com.thinkascoder.oop.week2._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Бывают такие классы в иерархии, которых просто не существует в жизни.
 */
public class AbstractClassHandsOn extends HandsOnRunner {
    // Посмотри на эту иерархию классов
    // Получилась стройная структура, только ведь в жизни не существует обьекта "абстрактная машина"(Message).
    // В программе не должно существовать обьектов типа Car, ведь не ясно, как на ней ехать?
    // Можно ехать на каене или x6, но как ехать на "Машине"?

    // Поэтому мы помечаем класс как абстрактный
    abstract class Car {
        public String brand;
    }

    class PorscheCayenne extends Car {
        public PorscheCayenne() {
            brand = "Porsche";
        }
    }

    class BmwX6 extends Car {
        public BmwX6() {
            brand = "BMW";
        }
    }

    @HandsOn("Абстрактные классы запрещают создание обьектов их типа")
    public void abstract_class_example() {
        BmwX6 bmwX6 = new BmwX6();
        PorscheCayenne porscheCayenne = new PorscheCayenne();
        // Компилятор следит, что бы мы не создали "абстрактный обьект"
        //Message car = new Message();

        assertEquals(bmwX6.brand, ___);
        assertEquals(porscheCayenne.brand, ___);
    }

    @HandsOn("У абстрактного класса есть конструктор как у обычного класса")
    public void abstract_class_has_constructor() {
        abstract class Person {
            public String name;

            public Person(String name) {
                this.name = name;
            }
        }

        class Singer extends Person {
            public Singer(String name) {
                super(name);
            }
        }

        Singer singer = new Singer("Michael Jackson");
        assertEquals(singer.name, ___);
    }

    @HandsOn("У абстрактного класса может быть абстрактный метод, " +
            "который имеет лишь обещание, что все конкретные классы его реализуют. Это называется контракт")
    public void abstract_method_example() {
        abstract class Vehicle {
            public StringBuffer appender = new StringBuffer();

            abstract void move();
        }

        class Car extends Vehicle {
            @Override
            void move() {
                appender.append("Message is moving");
            }
        }

        class Bicycle extends Vehicle {
            @Override
            void move() {
                appender.append("Bicycle is moving");
            }
        }

        /*

        //Раскоментируй эти строки и реализуй класс мотоцикл, обрати внимание, что компилятор заставляет тебя реализовать
        // все абстрактные методы или сделать свой класс абстрактным
        class Motorcycle extends Vehicle {

        }
        */

        Car car = new Car();
        car.move();
        assertEquals(car.appender.toString(), ___);

        Bicycle bicycle = new Bicycle();
        bicycle.move();
        assertEquals(bicycle.appender.toString(), ___);

        // Раскоментируй, что бы проходил тест
        /*
          Motorcycle motorcycle = new Motorcycle();
          motorcycle.move();
          assertEquals(motorcycle.appender.toString(), ___);
         */
    }

    // call from non abstract -> abstract
}
