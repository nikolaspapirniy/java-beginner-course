package com.thinkascoder.oop.week2._6;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MessagesManagerTest {
    public static final String TWITTER_MESSAGE_CLASS_NAME = "TwitterMessage";
    public static final String OK_MESSAGE_CLASS_NAME = "OkMessage";

    private MessagesManager messagesManager;

    // Метод c @Before вызывается перед каждым тестовым методом
    @Before
    public void setUp() throws Exception {
        // перед каждым тестом мы пересоздаем автосалон
        messagesManager = new MessagesManager();
    }

    @Test
    public void count_should_show_zero_if_no_messages() throws Exception {
        assertEquals(0, messagesManager.countOfMessages());
    }

    @Test
    public void count_should_show_one_if_only_one_message_from_vm() throws Exception {
        fail("Пока не реализовал");
    }

    @Test
    public void count_should_show_one_if_only_one_message_from_facebook() throws Exception {
        fail("Пока не реализовал");
    }

    @Test
    public void count_should_show_two_if_vk_and_facebook_messages_are_present() throws Exception {
        fail("Пока не реализовал");
    }

    @Test
    public void getAllMessages_should_return_empty_array_if_no_messages() throws Exception {
        assertArrayEquals(new String[0], messagesManager.getAllMessages());
    }

    @Test
    public void getAllMessages_should_return_one_vk_message() throws Exception {
        fail("Пока не реализовал");
    }

    @Test
    public void getAllMessages_should_return_one_facebook_message() throws Exception {
        fail("Пока не реализовал");
    }

    @Test
    public void getAllMessages_should_return_one_facebook_and_one_vk_messages() throws Exception {
        fail("Пока не реализовал");
    }
}
