package com.thinkascoder.oop.week2._6._7_8_9;

import com.thinkascoder.oop.week2._6.Message;
import com.thinkascoder.oop.week2._6.MessagesManager;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.*;
import static com.thinkascoder.system.validations.MethodValidations.*;

public class MoveToPolymorphismSignatureTest {
    @Test
    public void messages_array_should_be_present_in_MessagesManager() throws Exception {
        assertHasField(MessagesManager.class, "messages", Message[].class);
        assertPrivateField(MessagesManager.class, "messages");
    }

    @Test
    public void messages_count_should_should_be_present_in_MessagesManager() throws Exception {
        assertHasField(MessagesManager.class, "countOfMessages", int.class);
        assertPrivateField(MessagesManager.class, "countOfMessages");
    }

    @Test
    public void class_specific_fields_should_be_removed_from_MessagesManager() throws Exception {
        assertHasNoField(MessagesManager.class, "vkMessages");
        assertHasNoField(MessagesManager.class, "facebookMessages");
        assertHasNoField(MessagesManager.class, "twitterMessages");

        assertHasNoField(MessagesManager.class, "countOfVkMessages");
        assertHasNoField(MessagesManager.class, "countOfFacebookMessages");
        assertHasNoField(MessagesManager.class, "countOfTwitterMessages");
    }

    @Test
    public void class_specific_methods_should_be_removed_from_MessagesManager() throws Exception {
        assertHasNoMethod(MessagesManager.class, "addVkMessage");
        assertHasNoMethod(MessagesManager.class, "addFacebookMessage");
        assertHasNoMethod(MessagesManager.class, "addTwitterMessage");
    }

    @Test
    public void polymorphic_addMessage_should_be_present_in_MessagesManager() throws Exception {
        assertHasMethod(MessagesManager.class, "addMessage");
        assertMethodIsPublic(MessagesManager.class, "addMessage");
        assertMethodParams(MessagesManager.class, "addMessage", Message.class);
    }
}
