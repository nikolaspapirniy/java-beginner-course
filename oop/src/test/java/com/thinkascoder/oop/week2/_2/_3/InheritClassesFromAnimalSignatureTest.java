package com.thinkascoder.oop.week2._2._3;

import com.thinkascoder.oop.week2._2.Animal;
import com.thinkascoder.oop.week2._2.Cat;
import com.thinkascoder.oop.week2._2.Dog;
import com.thinkascoder.oop.week2._2.Fox;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertInheritance;

// Давай убедимся, что ты наследовал все верно
public class InheritClassesFromAnimalSignatureTest {
    @Test
    public void cat_should_inherit_from_animal() throws Exception {
        assertInheritance(Animal.class, Cat.class);
    }

    @Test
    public void dog_should_inherit_from_animal() throws Exception {
        assertInheritance(Animal.class, Dog.class);
    }

    @Test
    public void fox_should_inherit_from_animal() throws Exception {
        assertInheritance(Animal.class, Fox.class);
    }
}
