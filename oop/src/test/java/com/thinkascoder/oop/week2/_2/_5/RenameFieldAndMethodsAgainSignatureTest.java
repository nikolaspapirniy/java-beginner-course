package com.thinkascoder.oop.week2._2._5;

import com.thinkascoder.oop.week2._2.Animal;
import com.thinkascoder.oop.week2._2.Cat;
import com.thinkascoder.oop.week2._2.Dog;
import com.thinkascoder.oop.week2._2.Fox;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertHasNoField;

// Давай проверим, все ли ты верно переименовал
public class RenameFieldAndMethodsAgainSignatureTest {
    public static final String NAME_FIELD = "petName";
    public static final String RENAMED_NAME_FIELD = "name";

    @Test
    public void old_name_field_should_be_removed_from_all_classes() throws Exception {
        assertHasNoField(Animal.class, NAME_FIELD);
        assertHasNoField(Dog.class, NAME_FIELD);
        assertHasNoField(Cat.class, NAME_FIELD);
        assertHasNoField(Fox.class, NAME_FIELD);
    }

    @Test
    public void pet_name_should_be_added() throws Exception {
        assertHasField(Animal.class, RENAMED_NAME_FIELD, String.class);
        assertHasNoField(Dog.class, RENAMED_NAME_FIELD);
        assertHasNoField(Cat.class, RENAMED_NAME_FIELD);
        assertHasNoField(Fox.class, RENAMED_NAME_FIELD);
    }
}
