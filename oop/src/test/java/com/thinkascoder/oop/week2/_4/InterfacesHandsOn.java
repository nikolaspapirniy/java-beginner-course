package com.thinkascoder.oop.week2._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

/**
 * Интерфейс подобен пользовательскому интерфейсу телефона, это неписанный контракт о том, что и как будет работать.
 * Интерфейсом к машине может выступать набор договоренностей:
 * - нажмешь на педаль газа - машина поедет
 * - нажмешь на педаль тормоза - машина остановится
 * - повернешь руль влево - машина поедет влево
 * - повернешь руль вправо - машина поедет вправо
 * <p>
 * Каждый класс имеет свой публичный интерфейс, это набор публичных методов. Так же класс имеет реализацию.
 * Бывают ситуации, когда у разных классов(в разной иерархии) может быть схожий интерфейс,
 * например, можно открывать(open) дверь машины, а еще дверь стиральной машины, а еще входную дверь.
 * <p>
 * Для этого нам помогут интерфейсы
 * (обрати внимание, что интерфейсы обычно называются прилогательными)
 */
public class InterfacesHandsOn extends HandsOnRunner {
    // Управляемый
    interface Driven {
        // все методы в интерфейсе автоматически становятся public abstract
        public abstract void drive();
    }

    // Заправляемый
    interface Fillable {
        // public abstract можно не писать, тогда компилятор сам подставит public abstract
        void fill();
    }

    // Обрати внимание, что для того, что бы имплементировать интерфейс используется ключевое слово implements(а не extends)
    class Bike implements Driven {
        @Override
        public void drive() {
            System.out.println("Еду на велике");
        }
    }

    // Можно имплементировать несколько интерфейсов(но нельзя наследоваться от нескольких классов)
    class Car implements Driven, Fillable {
        @Override
        public void drive() {
            System.out.println("Еду на машине");
        }

        @Override
        public void fill() {
            System.out.println("Заправились");
        }
    }

    // Реализуй ниже класс трактор
    // class Tractor implements Driven, Fillable {
    // }

    @HandsOn("interface - это специальная конструкция похожая на абстрактный класс, где все методы публичные и абстрактные")
    public void interface_example() {
        // Tractor tractor = new Tractor();
        // tractor.drive();
        // tractor.fill();

        fail("Удали, когда реализуешь трактор");
    }

    // Сортирующий
    interface Sortable {
        //не принято писать public abstract
        int[] sort(int[] array);
    }

    class Array implements Sortable {
        @Override
        public int[] sort(int[] array) {
            return new int[0];
        }
    }

    class Queue implements Sortable {
        @Override
        public int[] sort(int[] array) {
            return new int[1];
        }
    }

    @HandsOn("Интерфейсы используются, когда мы хотим показать, что разные классы обладают одинаковым поведением. " +
            "Они заставляют нас реализовывать методы")
    public void interface_example_2() {
        Array array = new Array();
        Queue queue = new Queue();

        assertEquals(array.sort(new int[]{1, 2, 3}), ___);
        assertEquals(queue.sort(new int[]{1, 2, 3}), ___);
    }

    interface Closeable {
        // все константы автоматически получают модификаторы public static final.
        public static final int TIMEOUT = 30;

        // принято их не писать, компилятор подставит их сам
        int CLOSE_AFTER = 30;
    }

    @HandsOn("В интерфейсах так же можно описывать константы помимо абстрактных методов")
    public void constants_in_interface() {
        assertEquals(Closeable.TIMEOUT, ___);
        assertEquals(Closeable.CLOSE_AFTER, ___);
    }
}
