package com.thinkascoder.oop.week2._2._4;

import com.thinkascoder.oop.week2._2.Animal;
import com.thinkascoder.oop.week2._2.Cat;
import com.thinkascoder.oop.week2._2.Dog;
import com.thinkascoder.oop.week2._2.Fox;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.*;

// Давай проверим, что ты убрал все дубликаты
public class RemoveMethodsAndFieldSignatureTest {
    public static final String CALL_OUT_METHOD_NAME = "callOut";
    public static final String GETTER_METHOD_NAME = "getPetName";

    // Call out
    @Test
    public void animal_should_have_call_out_method() throws Exception {
        assertHasMethod(Animal.class, CALL_OUT_METHOD_NAME);
    }

    @Test
    public void cat_should_inherit_call_out_method() throws Exception {
        assertHasNoMethod(Cat.class, CALL_OUT_METHOD_NAME);
        assertHasInheritedMethod(Cat.class, CALL_OUT_METHOD_NAME);
    }

    @Test
    public void dog_should_inherit_call_out_method() throws Exception {
        assertHasNoMethod(Dog.class, CALL_OUT_METHOD_NAME);
        assertHasInheritedMethod(Dog.class, CALL_OUT_METHOD_NAME);
    }

    @Test
    public void fox_should_inherit_call_out_method() throws Exception {
        assertHasNoMethod(Fox.class, CALL_OUT_METHOD_NAME);
        assertHasInheritedMethod(Fox.class, CALL_OUT_METHOD_NAME);
    }

    // Getter
    @Test
    public void animal_should_have_getter_method() throws Exception {
        assertHasMethod(Animal.class, GETTER_METHOD_NAME);
    }

    @Test
    public void cat_should_inherit_getter_method() throws Exception {
        assertHasNoMethod(Cat.class, GETTER_METHOD_NAME);
        assertHasInheritedMethod(Cat.class, GETTER_METHOD_NAME);
    }

    @Test
    public void dog_should_inherit_getter_method() throws Exception {
        assertHasNoMethod(Dog.class, GETTER_METHOD_NAME);
        assertHasInheritedMethod(Dog.class, GETTER_METHOD_NAME);
    }

    @Test
    public void fox_should_inherit_getter_method() throws Exception {
        assertHasNoMethod(Fox.class, GETTER_METHOD_NAME);
        assertHasInheritedMethod(Fox.class, GETTER_METHOD_NAME);
    }
}
