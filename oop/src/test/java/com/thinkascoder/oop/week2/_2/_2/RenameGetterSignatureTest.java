package com.thinkascoder.oop.week2._2._2;

import com.thinkascoder.oop.week2._2.Animal;
import com.thinkascoder.oop.week2._2.Cat;
import com.thinkascoder.oop.week2._2.Dog;
import com.thinkascoder.oop.week2._2.Fox;
import org.junit.Test;

import static com.thinkascoder.system.validations.MethodValidations.assertHasMethod;
import static com.thinkascoder.system.validations.MethodValidations.assertHasNoMethod;

// Давай убедимся, что ты переименовал все верно
public class RenameGetterSignatureTest {
    public static final String OLD_GETTER_NAME = "getName";
    public static final String NEW_GETTER_NAME = "getPetName";

    @Test
    public void old_getter_should_be_removed() throws Exception {
        assertHasNoMethod(Animal.class, OLD_GETTER_NAME);
        assertHasNoMethod(Dog.class, OLD_GETTER_NAME);
        assertHasNoMethod(Cat.class, OLD_GETTER_NAME);
        assertHasNoMethod(Fox.class, OLD_GETTER_NAME);
    }

    @Test
    public void new_getter_should_be_added() throws Exception {
        assertHasMethod(Animal.class, NEW_GETTER_NAME);
        assertHasMethod(Dog.class, NEW_GETTER_NAME);
        assertHasMethod(Cat.class, NEW_GETTER_NAME);
        assertHasMethod(Fox.class, NEW_GETTER_NAME);
    }
}
