package com.thinkascoder.oop.week2._6._3;

import com.thinkascoder.oop.week2._6.MessagesManager;
import com.thinkascoder.oop.week2._6.MessagesManagerTest;
import org.junit.Test;

import java.lang.reflect.Array;

import static com.thinkascoder.system.validations.ClassValidations.assertClass;
import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertPrivateField;
import static com.thinkascoder.system.validations.MethodValidations.*;

public class AddTwitterMessageSignatureTest {
    @Test
    public void messageManager_should_have_array_to_hold_messages() throws Exception {
        Class twitterMessageClass = assertClass(MessagesManagerTest.TWITTER_MESSAGE_CLASS_NAME);
        Object arrayOfTwitterMessage = Array.newInstance(twitterMessageClass, 0);

        assertHasField(MessagesManager.class, "twitterMessages", arrayOfTwitterMessage.getClass());
        assertPrivateField(MessagesManager.class, "twitterMessages");
    }

    @Test
    public void messageManager_should_have_countOfTwitterMessages_to_hold_count_of_messages() throws Exception {
        assertHasField(MessagesManager.class, "countOfTwitterMessages", int.class);
    }

    @Test
    public void messageManager_should_have_addTwitterMessage_method() throws Exception {
        Class twitterMessageClass = assertClass(MessagesManagerTest.TWITTER_MESSAGE_CLASS_NAME);

        assertHasMethod(MessagesManager.class, "addTwitterMessage");
        assertMethodIsPublic(MessagesManager.class, "addTwitterMessage");
        assertMethodParams(MessagesManager.class, "addTwitterMessage", twitterMessageClass);
        assertMethodReturnsNothing(MessagesManager.class, "addTwitterMessage");
    }
}
