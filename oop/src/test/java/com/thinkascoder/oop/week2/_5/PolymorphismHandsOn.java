package com.thinkascoder.oop.week2._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/*
 * Полиморфизм - это третий и завершающий элемент ООП (после инкапсуляции и наследования)
 * Он говорит, что мы можем обращаться с обьектами, не зная их конкретный тип.
 */
public class PolymorphismHandsOn extends HandsOnRunner {
    public static int method(int a, int b) {
        int y = 0;
        if (a > 0 && b > 0) {
            y = 10;
        } else {
            y = 20;
        }

        return y;
    }

    @HandsOn("Принцип полиморфизма позволяет привести тип любого наследника к типу родителя")
    public void implicit_type_casting() {
        // Теперь мы смотрим на мальчика как на человека, а это значит что теперь мы можем вызывать только методы из Person
        Person person = (Person) new Boy("Игорь");

        assertEquals(person.getName(), ___);
        // А здесь будет ошибка компиляции, потому что в классе Person нет метода playWithCars
        //person.playWithCars();
    }

    @HandsOn("Полиморфизм позволяет нам обращатся с обьектами не зная какой у них тип")
    public void polymorphism_without_type_knowladge() {
        class PersonNameToUpperCaseConverter {
            // Этому методу не важно кто скрывается за человеком девочка или мальчик, код работает для них одинаково
            public String personNameToUpperCase(Person person) {
                return person.getName().toUpperCase();
            }
        }

        // Поскольку мальчик является человеком, явное приведение типа можно убрать
        Person person1 = new Boy("Аристарх");

        // Поскольку девочка тоже является человеком, явное приведение типа можно убрать
        Person person2 = new Girl("Агрипина");

        PersonNameToUpperCaseConverter converter = new PersonNameToUpperCaseConverter();

        assertEquals(converter.personNameToUpperCase(person1), ___);
        assertEquals(converter.personNameToUpperCase(person2), ___);
    }

    @HandsOn("Не любой обьект человека явяется девочкой, поэтому нужно явное приведение типа")
    public void explicit_type_casting() {
        Person person1 = new Boy("Володя Шарапов");
        Person person2 = new Girl("Алина Кабаева");

        // Если написать
        // Boy boy = person1;
        // то будет ошибка компиляции
        // ведь за person1 может скрываться девочка
        Boy boy = (Boy) person1;
        Girl girl = (Girl) person2;

        // а теперь конкретные методы доступны потому, что ссылка конкретного типа
        assertEquals(boy.playWithCars(), ___);
        assertEquals(girl.playWithDolls(), ___);
    }

    @HandsOn("Вся прелесть полиморфизма проявляется при работе с коллекциями обьектов")
    public void polymorphism_with_arrays() {
        Person[] people = new Person[2];
        people[0] = new Boy("Чак Норрис");
        people[1] = new Girl("Джастин Бибер");

        StringBuilder sb = new StringBuilder();
        for (Person p : people) {
            sb.append(p.getName());
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn("Любой класс неявно наследуется от Object")
    public void every_class_is_object() {
        Object o = new Boy("Володя Шарапов");
        Boy boy = (Boy) o;

        assertEquals(boy.getName(), ___);
    }

    class Person {
        private String name;

        public Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    class Boy extends Person {
        public Boy(String name) {
            super(name);
        }

        public String playWithCars() {
            return "Играю с машинками";
        }
    }

    class Girl extends Person {
        public Girl(String name) {
            super(name);
        }

        public String playWithDolls() {
            return "Играю с куколками";
        }
    }
}
