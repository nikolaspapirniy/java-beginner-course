package com.thinkascoder.oop.week2._6._2;

import com.thinkascoder.oop.week2._6.Message;
import com.thinkascoder.oop.week2._6.MessagesManagerTest;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.*;
import static com.thinkascoder.system.validations.ClassValidations.assertInheritance;

public class TwitterMessageSignatureTest {
    @Test
    public void class_TwitterMessage_should_be_created_and_inherited_from_Message() throws Exception {
        assertInheritance(Message.class, MessagesManagerTest.TWITTER_MESSAGE_CLASS_NAME);
    }
}
