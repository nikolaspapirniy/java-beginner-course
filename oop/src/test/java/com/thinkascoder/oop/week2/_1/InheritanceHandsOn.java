package com.thinkascoder.oop.week2._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

/**
 * Наследование- это базовый принцип в ООП, который говорит, что один класс может расширять другой класс,
 * добавляя более конкретное состояние и поведение.
 * Он говорит, что можно создать более специализированную версию класса.
 * Например, Чайка - это более специализированная версия Птицы. Iphone - это частный случай Телефона.
 * При наследовании наследник получает поля и методы родителя. Это помогает строить иирархии и уменьшать дублирование кода.
 * При наследовании образуется отношение, которое называют "является"(is-a), между 2 классами.
 * Например, любой стеклянный стол (GlassTable) является столом(Table), любой грузовик(Track) является транспортом(Vehicle).
 * Но в обратную сторону это отношение не работает, то есть не любой сотрудник компании(Employee) является ген директором(CEO).
 */
public class InheritanceHandsOn extends HandsOnRunner {
    @HandsOn("При наследовании класс наследник автоматически получает все публичные поля своего родителя")
    public void public_fields_are_inherited() {
        Ebook harryPotterEbook = new Ebook();
        harryPotterEbook.downloadLink = "http://bez-regestracii-i-sms.com";
        harryPotterEbook.sizeInMb = 23.5;

        // обрати внимание, что в классе Ebook нет этих полей, они достались по наследству
        harryPotterEbook.author = "J.K. Rowling";
        harryPotterEbook.name = "Harry Potter";

        // а вот приватные поля не наследуются
        // раскоментируй, чтобы убедиться, что будет ошибка компиляции
        // harryPotterEbook.isbn = 123456789;

        assertEquals(harryPotterEbook.downloadLink, ___);
        assertEquals(harryPotterEbook.sizeInMb, ___);
        assertEquals(harryPotterEbook.author, ___);
        assertEquals(harryPotterEbook.name, ___);
    }

    @HandsOn("Класс наследник автоматически получает все публичные методы, например, геттеры и сеттеры")
    public void methods_inheritance_example() {
        Ebook thinkingInJavaEbook = new Ebook();
        thinkingInJavaEbook.downloadLink = "http://www.amazon.com/";
        thinkingInJavaEbook.sizeInMb = 13.5;
        thinkingInJavaEbook.author = "Bruce Eckel";
        thinkingInJavaEbook.name = "Thinking in java";

        // этого метода нет в Ebook
        thinkingInJavaEbook.setIsbn(123456789);

        // обрати внимание, что приватный метод не наследуется
        // раскоментируй, чтобы убедиться, что будет ошибка компиляции
        // int convertedIsbn = thinkingInJavaEbook.convertIsbn(0);

        // Посмотри на реализацию getIsbn
        // внутри getIsbn, метод convertIsbn доступен. Потому что private метод доступен только внутри того же класса

        assertEquals(thinkingInJavaEbook.getIsbn(), ___);
    }

    @HandsOn("Поскольку класс наследник является более конкретной версией класса родителя, " +
            "можно переопределить поведение на более конкретное")
    public void methods_overriding() {
        class Company {
            public String getLegalType() {
                return "ООО";
            }

            public String getAddress() {
                return "Где-то на земле";
            }
        }

        class OOOCompany extends Company {
            // переопределнные методы помечаются @Override
            @Override
            public String getAddress() {
                return "USA. San Francisco";
            }
        }

        OOOCompany oooCompany = new OOOCompany();
        // этот метод унаследован от Company
        assertEquals(oooCompany.getLegalType(), ___);

        // этот метод переопределен в более конкретном классе
        assertEquals(oooCompany.getAddress(), ___);
    }

    @HandsOn("Модификатор protected - как private, только доступен еще и у наследников")
    public void is_a_example() {
        class IPhone {
            protected int serialVersion;

            protected int makeSerialVersion() {
                // сложные вычисления
                return serialVersion * 2;
            }
        }

        class IPhone5S extends IPhone {
            public IPhone5S() {
                // поле унаследовано от родителя
                serialVersion = 12;
            }

            public String getMyPrettySerialVersion() {
                // вызываем метод робителя
                return "Ваш serialVersion = " + makeSerialVersion();
            }
        }

        assertEquals(new IPhone5S().getMyPrettySerialVersion(), ___);
        // эта строка не скомпилируется потому, что за пределами класса IPhone5S поле не доступно
        // int serialVersion = new IPhone5S().serialVersion;
    }

    @HandsOn("При конструировании обьекта вызываются конструкторы всей иерархии, " +
            "что бы гарантировать корректную инициализацию обьекта на каждом уровне")
    public void constructors_invocation_in_inheritance_example() {
        class Person {
            public StringBuilder constructorsRegistration = new StringBuilder();

            public Person() {
                constructorsRegistration.append("Person_constructor");
            }
        }

        class Male extends Person {
            public Male() {
                constructorsRegistration.append("Male_constructor");
            }
        }

        class AmericanMale extends Male {
            public AmericanMale() {
                constructorsRegistration.append("AmericanMale_constructor");
            }
        }

        AmericanMale americanMale = new AmericanMale();

        assertEquals(americanMale.constructorsRegistration, ___);
    }

    @HandsOn("Во время вызова конструктора в иирархии компилятор неявно вызывает конструктор своего родителя")
    public void invoke_super_explicitly() {

        class Person {
            public StringBuilder constructorsRegistration = new StringBuilder();

            public Person() {
                // а ведь вроде класс Person ни от кого не наследуестся... хм (в следующих заданиях ты поймешь, почему так)
                super();
                constructorsRegistration.append("Person_constructor");
            }
        }

        class Female extends Person {
            public Female() {
                // ключевое слово super - ссылка на класс родителя, а super() - вызов конструктора родителя
                super();
                constructorsRegistration.append("Female_constructor");
            }
        }

        Female female = new Female();
        assertEquals(female.constructorsRegistration.toString(), ___);
    }

    @HandsOn("Можно вызывать конструктор родителя с параметрами")
    public void invoke_parent_constructor_with_params() {
        class Person {
            public String name;
            public String gender;

            public Person(String name) {
                this.name = name;
            }
        }

        class Female extends Person {
            public Female(String name) {
                super(name);
                gender = "Female";
            }
        }

        Female female = new Female("Аня");
        assertEquals(female.name, ___);
        assertEquals(female.gender, ___);
    }

    static class Book {
        public String author;
        public String name;
        private int isbn = -1;

        public int getIsbn() {
            return convertIsbn(isbn);
        }

        public void setIsbn(int isbn) {
            this.isbn = isbn;
        }

        private int convertIsbn(int isbn) {
            // здесь сложные вычесления
            return isbn * 2;
        }
    }

    static class Ebook extends Book {
        public String downloadLink;
        public double sizeInMb;
    }
}
