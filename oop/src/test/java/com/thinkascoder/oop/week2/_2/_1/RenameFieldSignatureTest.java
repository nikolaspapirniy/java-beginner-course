package com.thinkascoder.oop.week2._2._1;

import com.thinkascoder.oop.week2._2.Animal;
import com.thinkascoder.oop.week2._2.Cat;
import com.thinkascoder.oop.week2._2.Dog;
import com.thinkascoder.oop.week2._2.Fox;
import org.junit.Test;

import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertHasNoField;

// Давай убедимся, что ты переименовал все верно
public class RenameFieldSignatureTest {
    public static final String NAME_FIELD = "name";
    public static final String RENAMED_NAME_FIELD = "petName";

    @Test
    public void name_field_should_be_removed() throws Exception {
        assertHasNoField(Animal.class, NAME_FIELD);
        assertHasNoField(Dog.class, NAME_FIELD);
        assertHasNoField(Cat.class, NAME_FIELD);
        assertHasNoField(Fox.class, NAME_FIELD);
    }

    @Test
    public void pet_name_should_be_added() throws Exception {
        assertHasField(Animal.class, RENAMED_NAME_FIELD, String.class);
        assertHasField(Dog.class, RENAMED_NAME_FIELD, String.class);
        assertHasField(Cat.class, RENAMED_NAME_FIELD, String.class);
        assertHasField(Fox.class, RENAMED_NAME_FIELD, String.class);
    }
}
