package com.thinkascoder.oop.week2._6._10;

import com.thinkascoder.oop.week2._6.Message;
import com.thinkascoder.oop.week2._6.MessagesManagerTest;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertInheritance;

// здесь писать ничего не нужно
public class AddOkMessageSignatureTest {
    @Test
    public void class_OkMessage_should_be_created_and_inherited_from_Message() throws Exception {
        assertInheritance(Message.class, MessagesManagerTest.OK_MESSAGE_CLASS_NAME);
    }
}
