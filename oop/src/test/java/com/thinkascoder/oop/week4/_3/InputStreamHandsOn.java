package com.thinkascoder.oop.week4._3;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.*;

public class InputStreamHandsOn extends HandsOnRunner {
    public static final String PATH_TO_RESOURCES_WEEK4_FOLDER = "src" + File.separator +
            "test" + File.separator +
            "resources" + File.separator +
            "com" + File.separator +
            "thinkascoder" + File.separator +
            "oop" + File.separator +
            "week4" + File.separator +
            "input_stream" + File.separator;

    @HandsOn("Поток(stream) это труба между java программой и (например)файлом. Тебе не нужно заботиться, как читать файл из" +
            "файловой системы, ты просто можешь обращаться к интерфейсу InputStream, который инкапсулирует работу с потоками")
    public void input_stream_example() {
        File fileToRead = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "file_with_text.txt");
        // открой этот файл что бы решить задачу
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();
        try {
            inputStream = new FileInputStream(fileToRead);

            // данные из потока читаются в буфер(массив байт)
            byte[] buffer = new byte[100];

            // read возвращает, сколько мы байт прочли за раз
            while (inputStream.read(buffer) != -1) {
                // что бы перевести байты в строку, воспользуемся удобным конструктором String
                String bytesAsString = new String(buffer).trim();
                sb.append(bytesAsString);
            }
        } catch (IOException e) {
            System.out.println("Ошибка при чтении");
        } finally {
            // После того как поработал с потоком, закрой его
            closeStream(inputStream);
        }

        assertEquals(sb.toString(), ___);
    }

    // Закрывать потоки дело утомительное, скоро ты узнаешь как разные библиотеки умеют закрывать потоки
    private void closeStream(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                System.out.println("Не получилось закрыть поток");
            }
        }
    }

    @HandsOn("FileInput загружает данные маленькими порциями, было бы лучше для производительности, если бы мы могли" +
            "читать и буферизировать большие куски данных. С этим нам поможет BufferedInputStream")
    public void buffer_input_stream_example() {
        StringBuilder sb = new StringBuilder();
        try {
            File fileToRead = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "file_with_text.txt");
            // BufferedInputStream читает сразу большими кусками, что улучшает производительность
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(fileToRead));
            byte[] buf = new byte[100];

            while (bufferedInputStream.read(buf) != -1) {
                sb.append(new String(buf, "UTF-8").trim());
            }
        } catch (IOException e) {
            System.out.println("Ошибка при чтении");
        }

        assertEquals(sb.toString(), ___);
    }

    @HandsOn("В этом задании тебе нужно прочесть файл read_file_yourself.txt с помощью BufferedInputStream")
    public void readFileYourself() {
        StringBuilder sb = new StringBuilder();
        // пиши код здесь

        assertEquals(sb.toString(), "read\n" + "file\n" + "yourself\n" + "with\n" + "input\n" + "stream");
    }
}
