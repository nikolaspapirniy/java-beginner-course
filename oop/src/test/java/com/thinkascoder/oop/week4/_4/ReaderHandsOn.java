package com.thinkascoder.oop.week4._4;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.*;

/**
 * Поток(stream) это труба между java программой и (например)файлом. Тебе не нужно заботиться как читать файл из
 * файловой системы, ты просто можешь обращаться к интерфейсу класса Reader, который инкапсулирует работу с потоками
 */
public class ReaderHandsOn extends HandsOnRunner {
    public static final String PATH_TO_RESOURCES_WEEK4_FOLDER = "src" + File.separator +
            "test" + File.separator +
            "resources" + File.separator +
            "com" + File.separator +
            "thinkascoder" + File.separator +
            "oop" + File.separator +
            "week4" + File.separator +
            "reader" + File.separator;

    @HandsOn("Reader позволяет нам посимвольно считывать файл(не byte, а char)")
    public void reader_example() throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();

        File fileToRead = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "file_for_reader.txt");
        // открой файл что бы решить эту задачу

        Reader reader = null;
        try {
            reader = new FileReader(fileToRead);

            // данные из потока читаются в буфер(массив символов)
            char[] buffer = new char[100];

            // read возвращает сколько мы байт прочли за раз
            while (reader.read(buffer) != -1) {
                // что бы перевести символы в строку, воспользуемся удобным конструктором String
                String bytesAsString = new String(buffer).trim();
                sb.append(bytesAsString);
            }
        } catch (IOException e) {
            System.out.println("Ошибка при чтении");
        } finally {
            // После того как поработал с потоком, закрой его
            // Закрывать потоки дело утомительное, скоро ты узнаешь как разные библиотеки умеют закрывать потоки
            closeReader(reader);
        }

        assertEquals(sb.toString(), ___);
    }

    private void closeReader(Reader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println("Не получилось закрыть поток");
            }
        }
    }

    @HandsOn("Читать посимвольно не очень удобно, было бы здорово если бы мы могли читать построчно. " +
            "BufferedReader предоставляет такую возможность. Он буферизирует чтение и может возвращать нам целые строки")
    public void reader_sd_example() {
        StringBuilder sb = new StringBuilder();

        File fileToRead = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "file_for_reader.txt");
        // открой файл что бы решить эту задачу
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileToRead));

            // читать построчно явно веселее
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            System.out.println("Ошибка во время чтения");
        } finally {
            // как всегда, не забываем закрыть поток
            closeReader(reader);
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn("В этом задании тебе нужно прочесть файл read_file_yourself.txt с помощью BufferedReader")
    public void readFileYourself() {
        StringBuilder sb = new StringBuilder();
        // пиши код здесь

        assertEquals(sb.toString(), "read\n" + "file\n" + "yourself\n" + "with\n" + "reader");
    }

}
