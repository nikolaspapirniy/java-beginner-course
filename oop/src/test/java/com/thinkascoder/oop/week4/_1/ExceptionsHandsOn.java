package com.thinkascoder.oop.week4._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.IOException;

import static org.junit.Assert.fail;


public class ExceptionsHandsOn extends HandsOnRunner {
    public int saveDataToDatabase(String data) {
        // сохраняю данные в базу данных
        boolean isError = true;
        if (isError)
            return -1;
        else
            return 1;
    }

    @HandsOn("Ошибки случаются часто и мы должны их корректно обрабатывать, но что возвращать когда появляется ошибка?")
    public void handle_errors_without_exceptions() {
        int saveDataToDatabaseResult = saveDataToDatabase("Hello");

        if (saveDataToDatabaseResult == 1)
            System.out.println("Сохранили данные");
        else if (saveDataToDatabaseResult == -1)
            System.out.println("Произошла ошибка");

        fail("Идея возвращать -1 в случае ошибки явно не лучшая, потому что мы не знаем что конкретно произошло");
    }

    public String anotherWayToSaveToDB(String data) {
        boolean isError = true;
        if (isError)
            return "Error: не смог подключится к базе данных";
        else
            return "OK";
    }

    @HandsOn("Может нам возвращать строку и туда писать что случилось?")
    public void handle_errors_with_strings() {
        String result = anotherWayToSaveToDB("Hello");

        if (result.startsWith("Error"))
            System.out.println("Произошла ошибка: " + result.split("Error: "));
        else if (result.startsWith("OK"))
            System.out.println("Сохранили в базу значение");

        fail("И теперь нам придется так проверять каждый вызов метода?");
    }

    public String betterWayToSaveToDB(String data) {
        boolean isError = true;
        if (isError) {
            // throw - ключевое слово, которое говорит что было брошено исключение
            throw new RuntimeException("Ошибка подключения к базе данных");
        } else {
            return "OK";
        }
    }

    @HandsOn("Исключение - это способ обработки внештатной ситуации, это попытка исправить положение. " +
            "Например, если нет подключения к интернету, мы можем попробовать переподключиться через 2 минуты. ")
    public void first_glance_of_exception() {
        String errorMessage = "";
        try {
            String data = betterWayToSaveToDB("data");
        } catch (RuntimeException e) {
            errorMessage = e.getMessage();
        }
        assertEquals(errorMessage, ___);
    }

    public double division(int a, int b) {
        if (b == 0)
            throw new IllegalArgumentException(); // это не проверяемое исключение

        return a / (double) b;
    }

    // throws показывает, что этот метод бросает проверяемое исключение.
    // Мы обязательно должны описать, что будем делать в случае
    public void saveDataToFile(String data) throws IOException {
        System.out.println("Сохраняю данные в файл");
        throw new IOException("Произошла ошибка при записи в файл");
    }

    @HandsOn("Исключение бывают проверяемые, это когда ты обязан обернуть вызов в try-catch")
    public void checked_exception() {
        // saveDataToFile("Data");
        fail("Раcкомментируй строку выше и оберни вызов метода в try-catch");
    }

    @HandsOn("Исключение так же бывают непроверяемые, это когда ты можешь ловить это исключение, а можешь нет")
    public void unchecked_exception() {
        StringBuilder sb = new StringBuilder();
        try {
            division(1, 0);
        } catch (IllegalArgumentException e) {
            // можно обрабатывать исключительную ситуацию
            sb.append("словил IllegalArgumentException");
        }

        // а можно и не обрабатывать, но тогда есть риск, что оно пробросится выше
        division(10, 5);
    }

    public void downloadVideoFromYoutube(String url) throws IOException {
        if (!url.startsWith("http")) {
            throw new IllegalArgumentException("Url к youtube должен начинаться с http");
        }

        // начал скачивать файл
        throw new IOException("Видимо отключился интернет, не могу больше скачивать");
    }

    @HandsOn("Ты можешь ловить и обрабатывать разные исключения по-разному")
    public void multiple_catches() {
        StringBuilder sb = new StringBuilder();
        // первая попытка
        try {
            downloadVideoFromYoutube("");
        } catch (IllegalArgumentException e) {
            sb.append("не верный url,");
        } catch (IOException e) {
            sb.append("ошибка подключения к интернету,");
        }

        // попробуем снова
        try {
            downloadVideoFromYoutube("http://youtube.com/123");
        } catch (IllegalArgumentException e) {
            sb.append("не верный url,");
        } catch (IOException e) {
            sb.append("ошибка подключения к интернету,");
        }

        assertEquals(sb.toString(), ___);
    }

    @HandsOn("Блок finally после блока catch")
    public void finally_block_exectes_after_catch() {
        StringBuilder sb = new StringBuilder();
        try {
            division(1, 0);
            sb.append("после деления,");
        } catch (RuntimeException e) {
            sb.append("деление на 0,");
        } finally {
            sb.append("блок finally,");
        }

        assertEquals(sb.toString(), ___);
    }

    @HandsOn("Блок finally так же отрабатывает, если исключение не было брошено")
    public void finally_block_exectes_even_without_exception() {
        StringBuilder sb = new StringBuilder();
        try {
            division(10, 1);
            sb.append("после деления,");
        } catch (RuntimeException e) {
            sb.append("деление на 0,");
        } finally {
            sb.append("блок finally,");
        }

        assertEquals(sb.toString(), ___);
    }

    @HandsOn("Блок finally так же отрабатывает, если в блоке try есть return")
    public void finally_block_executes_even_when_return_in_try() {
        StringBuilder sb = new StringBuilder();
        try {
            division(10, 1);
            sb.append("после деления,");
            return;
        } catch (RuntimeException e) {
            sb.append("деление на 0,");
        } finally {
            sb.append("блок finally,");
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn("Блок finally так же отрабатывает, если в блоке catch есть return")
    public void finally_block_executes_even_when_return_in_catch() {
        StringBuilder sb = new StringBuilder();
        try {
            division(10, 1);
            sb.append("после деления,");
        } catch (RuntimeException e) {
            sb.append("деление на 0,");
            return;
        } finally {
            sb.append("блок finally,");
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn(value = "Ты можешь использовать try-finally, когда тебе важно, чтобы блок finally отработал, " +
            "но обрабатывать исключение ты не хочешь")
    public void try_finally() {
        StringBuilder sb = new StringBuilder();
        try {
            try {
                division(10, 0);
                sb.append("после деления на 0,");
            } finally {
                sb.append("блок finally,");
            }
        } catch (IllegalArgumentException e) {
            // нас интерисует внутренний try-finally
        }
        assertEquals(sb.toString(), ___);
    }
}
