package com.thinkascoder.oop.week4._6;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.helpers.FileHelper;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.*;

import static com.thinkascoder.system.validations.FileValidations.assertFileExists;

public class WriterHandsOn extends HandsOnRunner {
    public static final String PATH_TO_RESOURCES_WEEK4_FOLDER = "src" + File.separator +
            "test" + File.separator +
            "resources" + File.separator +
            "com" + File.separator +
            "thinkascoder" + File.separator +
            "oop" + File.separator +
            "week4" + File.separator +
            "writer" + File.separator;

    @HandsOn("С помощью fileWriter можно писать строки, а не поток байт")
    public void writer_example() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "new_writer_file.txt");
        // удаляем, если уже создавали
        FileHelper.deleteFileIfExists(file);

        String message = "Это сообщение записано через fileWriter";

        Writer fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeWriter(fileWriter);
        }

        String readFromFile = FileHelper.readFromFile(file);
        assertEquals(readFromFile, ___);
    }

    @HandsOn("BufferedWriter позволяет нам эффективнее записывать в файл. Он накапливает в буфере больше данных, а потом" +
            "записывает их разом на файловую систему.")
    public void buffered_write_example() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "new_buffered_writer_file.txt");
        // удаляем, если уже создавали
        FileHelper.deleteFileIfExists(file);

        String message = "Это сообщение записано через bufferedWriter";

        Writer bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeWriter(bufferedWriter);
        }

        String readFromFile = FileHelper.readFromFile(file);
        assertEquals(readFromFile, ___);
    }

    @HandsOn("Попробуй сам записать текст в файл с помощью Writer")
    public void write_to_file_by_yourself() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "write_file_by_yourself.txt");
        String message = "Я записал это сообщение сам с помощью Writer";
        // пиши код ниже

        assertFileExists(file);
        String readFromFile = FileHelper.readFromFile(file);
        assertEquals(message, readFromFile);
    }

    private void closeWriter(Writer writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
