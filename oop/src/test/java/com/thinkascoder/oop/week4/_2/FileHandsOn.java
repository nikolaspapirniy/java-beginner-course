package com.thinkascoder.oop.week4._2;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileHandsOn extends HandsOnRunner {
    public static final String PATH_TO_RESOURCES_WEEK4_FOLDER = "src" + File.separator +
            "test" + File.separator +
            "resources" + File.separator +
            "com" + File.separator +
            "thinkascoder" + File.separator +
            "oop" + File.separator +
            "week4" + File.separator +
            "files" + File.separator;

    @HandsOn("File - это обьект дескриптор, который описывает файл, но это не файл в файловой системе." +
            "File, как паспорт человека, он описывает данные о нем, но паспорт это не человек")
    public void file_as_file_descriptor() {
        // Все разделители заменены на File.separator, потому что в windows разделитель \, а в linux /
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + "just_a_file.txt");
        // открой папку с файлом, что бы решить задачу

        assertEquals(PATH_TO_RESOURCES_WEEK4_FOLDER, ___);
        assertEquals(file.getPath(), ___);
        assertEquals(File.separator, ___);
        assertEquals(file.exists(), ___);
        assertEquals(file.getName(), ___);
    }

    @HandsOn("File так же может описывать директории(поскольку это всего навсего обьект, который описывает реальную директорию)")
    public void file_as_folder_descriptor() {
        File folder = new File(PATH_TO_RESOURCES_WEEK4_FOLDER);

        assertEquals(folder.getPath(), ___);
        assertEquals(folder.isFile(), ___);
        assertEquals(folder.exists(), ___);
        assertEquals(folder.getName(), ___);
    }

    @HandsOn("Можно создавать новые файлы или удалять их работая с интерфейсом File")
    public void file_api_create_remove() throws IOException {
        File newFile = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + "new_file.txt");

        newFile.createNewFile();

        if (newFile.exists()) {
            assertEquals(newFile.delete(), ___);
        }

        assertEquals(newFile.exists(), ___);
    }

    @HandsOn("Можно создавать новые файлы или удалять их работая с интерфейсом File")
    public void file_api_rename() throws IOException {
        File newFile = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + "another_new_file.txt");
        File renamedFile = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + "renamed_file.txt");
        // ----- удаляем файлы после теста -----
        newFile.deleteOnExit();
        renamedFile.deleteOnExit();
        // ---------------

        newFile.createNewFile();

        newFile.renameTo(renamedFile);


        assertEquals(newFile.exists(), ___);
        assertEquals(renamedFile.exists(), ___);
    }

    @HandsOn("Можно так же создавать папки")
    public void file_api_mkdir() throws IOException {
        File newFolder = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + "new_folder");
        // ----- удаляем папку после теста -----
        newFolder.deleteOnExit();
        // ---------------

        newFolder.mkdir();

        assertEquals(newFolder.exists(), ___);
    }

    @HandsOn("list - показывает список файлов в папке")
    public void file_api_list() throws IOException {
        File folder = new File(PATH_TO_RESOURCES_WEEK4_FOLDER);
        assertEquals(Arrays.toString(folder.list()), ___);
    }
}
