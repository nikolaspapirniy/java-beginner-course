package com.thinkascoder.oop.week4._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.helpers.FileHelper;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.io.*;

import static com.thinkascoder.system.validations.FileValidations.assertFileExists;

public class OutputStreamHandsOn extends HandsOnRunner {
    public static final String PATH_TO_RESOURCES_WEEK4_FOLDER = "src" + File.separator +
            "test" + File.separator +
            "resources" + File.separator +
            "com" + File.separator +
            "thinkascoder" + File.separator +
            "oop" + File.separator +
            "week4" + File.separator +
            "output_stream" + File.separator;

    @HandsOn("Так же как и InputStream, OutputStream - абстракция над записью данных в файл. " +
            "Вы просто передаете поток байт в метод write, а он сохранит данные на файловую систему")
    public void output_stream_example() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "new_outputstream_file.txt");
        // удаляем, если уже создавали
        FileHelper.deleteFileIfExists(file);

        String message = "Это сообщение будет сохранено";
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(message.getBytes());
        } catch (IOException e) {
            System.out.println("Ошибка при записи в файл");
        } finally {
            // не забываем закрыть поток
            closeOutputStream(outputStream);
        }

        String readMessageFromFile = FileHelper.readFromFile(file);
        assertEquals(readMessageFromFile, ___);
    }

    @HandsOn("FileOutputStream является не эффективным, потому что каждый раз как вы вызываете метод write, он пытается записать" +
            "данные на файловую систему. Было бы эффективнее писать более крупными блоками. Для этого есть BufferedOutputStream," +
            "в который мы обернем FileOutputStream")
    public void buffered_output_stream_example() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "new_buffered_outputstream_file.txt");
        // удаляем, если уже создавали
        FileHelper.deleteFileIfExists(file);

        String message = "Это сообщение тоже будет сохранено";

        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            bufferedOutputStream.write(message.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeOutputStream(bufferedOutputStream);
        }

        String readMessageFromFile = FileHelper.readFromFile(file);
        assertEquals(readMessageFromFile, ___);
    }

    @HandsOn("Попробуй сам записать текст в файл с помощью Writer")
    public void write_to_file_by_yourself() {
        File file = new File(PATH_TO_RESOURCES_WEEK4_FOLDER + File.separator + "write_file_by_yourself.txt");
        String message = "Я записал это сообщение сам с помощью OutputStream";
        // пиши код ниже

        assertFileExists(file);
        String readFromFile = FileHelper.readFromFile(file);
        assertEquals(message, readFromFile);
    }

    private void closeOutputStream(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
