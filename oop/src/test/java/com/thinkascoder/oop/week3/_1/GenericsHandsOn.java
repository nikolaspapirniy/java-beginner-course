package com.thinkascoder.oop.week3._1;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import static org.junit.Assert.fail;

public class GenericsHandsOn extends HandsOnRunner {
    @HandsOn("Возможно Object это слишком абстрактно?")
    public void example_without_generics() {
        // Скажем, у нас есть класс коробка, который просто хранит какое-то значение
        class Box {
            // Мы делаем тип ссылки на хранимый обьект - Object, чтобы можно было хранить значения разных типов
            private Object value;

            public Box(Object value) {
                this.value = value;
            }

            public Object getValue() {
                return value;
            }

            public void setValue(Object value) {
                this.value = value;
            }
        }

        Box[] box = new Box[5];
        // просим пользователя ввести числа
        box[0] = new Box(10);
        box[1] = new Box(15);
        box[2] = new Box(20);
        box[3] = new Box(25);
        box[4] = new Box("я не знаю следующего числа");
        // наша коробка позволяет добавить любой тип(потому что все классы в java наследуются от Object)


        for (int i = 0; i < box.length; i++) {
            Object valuesAsObject = box[i].getValue();
            double valueAsInteger = (Integer) valuesAsObject;
            // делаю вычисления
        }
        System.out.println("Почему эта строка не выведется?");
        // Может я добавил не правильный тип данных?
    }

    @HandsOn("Было бы здорово если бы мы могли контролировать тип данных в Box, но тогда на каждый тип надо писать свою реализацию")
    public void one_class_per_type() {
        // Присмотрись, это просто дублирование кода
        class BoxWithInteger {
            private int value;

            public BoxWithInteger(int value) {
                this.value = value;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }
        }

        class BoxWithString {
            private String value;

            public BoxWithString(String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        BoxWithInteger[] boxWithIntegers = new BoxWithInteger[3];
        // просим пользователя ввести числа
        boxWithIntegers[0] = new BoxWithInteger(10);
        boxWithIntegers[1] = new BoxWithInteger(15);
        boxWithIntegers[2] = new BoxWithInteger(20);
        // box[3] = new BoxWithInteger("я не знаю следующего числа");   // эта строка не скомпилируется


        BoxWithString[] boxWithStrings = new BoxWithString[3];
        // просим пользователя ввести строки
        boxWithStrings[0] = new BoxWithString("Иван");
        boxWithStrings[1] = new BoxWithString("Сережа");
        boxWithStrings[2] = new BoxWithString("Петя");

        int sumOfValues = 0;
        for (BoxWithInteger boxWithInteger : boxWithIntegers) {
            sumOfValues += boxWithInteger.getValue();
        }

        int sumOfLength = 0;
        for (BoxWithString boxWithString : boxWithStrings) {
            sumOfLength += boxWithString.getValue().length();
        }

        assertEquals(sumOfValues, ___);
        assertEquals(sumOfLength, ___);

        fail("Появилась проверка типа и мы теперь не можем засунуть значение не правильного типа, но теперь надо писать " +
                "классу на каждый тип данных, а это не хорошо");
    }

    @HandsOn("Одна переменная может хранить разные значения, почему же мы не можем сделать переменную для типа?")
    public void generic_example() {
        // Присмотрись к классам из предыдущего задания (BoxWithInteger, BoxWithString)
        // Обрати внимание, что эти 2 класса идентичны, отличается только int от String в 4 местах
        // А что если можно вынести этот тип в переменную?

        // <T> после имени класса говорит что наш класс параметризирован
        // Мы просто вынесли изменяемую часть(тип данных) в переменную
        class Box<T> {
            private T value;

            public Box(T value) {
                this.value = value;
            }

            public T getValue() {
                return value;
            }

            public void setValue(T value) {
                this.value = value;
            }
        }

        Box<Integer> box = new Box<Integer>(10);
        // Box<Integer> boxWithString = new Box<Integer>("hello");          // код не скомпилируется

        // Обрати внимание что Box возвращает не Object, а Integer(то что мы указали в <Integer>)
        Integer value = box.getValue();
        assertEquals(value, ___);
    }

    @HandsOn("В дженериках можно использовать только классы обертки")
    public void wrapper_types() {
        assertEquals(getWrapperType((int) 10), Integer.class);
        assertEquals(getWrapperType((short) 10), ___);
        assertEquals(getWrapperType((byte) 10), ___);
        assertEquals(getWrapperType((long) 10), ___);
        assertEquals(getWrapperType((double) 10), ___);
        assertEquals(getWrapperType((float) 10), ___);
        assertEquals(getWrapperType((char) 10), ___);
    }

    @HandsOn("Generic может использоваться в методе")
    public void generic_method() {
        // обрати внимание что с generic'ами можно работать только через типы обертки
        Integer[] ints = {1, 2, 3, 4, 5};
        Double[] doubles = {1.5, 2.5, 3.5, 6.5};

        assertEquals(arrayToString(ints), ___);
        assertEquals(arrayToString(doubles), ___);
    }

    // <E> говорит о том что в методе используется дженерик тип E(он используется в параметре)
    public <E> String arrayToString(E[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (E element : array) {
            stringBuilder.append(element);
        }
        return stringBuilder.toString();
    }

    @HandsOn("В одном описании может быть несколько Generic параметров")
    public void multiple_generic_params() {
        class KeyValue<K, V> {
            public K key;
            public V value;
        }

        // У обьекта можно не повторять дженерик типы, достаточно <>, компилятор увидит типу у ссылки
        KeyValue<Integer, String> facebookUser = new KeyValue<>();
        facebookUser.key = 012346;      // id пользователя
        facebookUser.value = "Сергей Дмитров";

        Integer key = facebookUser.key;
        String value = facebookUser.value;
        assertEquals(key, ___);
        assertEquals(value, ___);
    }

    @HandsOn("Generics не позволяют создать массив generic типа")
    public void generic_array_creation() {
        class Collection<E> {
            private final E[] objects;

            public Collection() {
                // Ты не можешь создать E[], секрет в том что бы создать Object[] а потом привести к массиву generic типа
                objects = (E[]) new Object[10];
            }

            public void set(int index, E value) {
                objects[index] = value;
            }

            public E get(int index) {
                return objects[index];
            }
        }

        Collection<String> stringCollection = new Collection<>();
        stringCollection.set(0, "microsoft");
        stringCollection.set(1, "ibm");

        assertEquals(stringCollection.get(0), ___);
        assertEquals(stringCollection.get(1), ___);
    }
}
