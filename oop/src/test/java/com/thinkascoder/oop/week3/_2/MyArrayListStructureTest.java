package com.thinkascoder.oop.week3._2;

import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertClassImplementsInterface;
import static com.thinkascoder.system.validations.ConstructorValidations.*;
import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertHasGenericField;
import static com.thinkascoder.system.validations.FieldValidations.assertPrivateField;

public class MyArrayListStructureTest {
    @Test
    public void should_have_generic_array_field() throws Exception {
        assertHasField(MyArrayList.class, "data", Object[].class);
        assertPrivateField(MyArrayList.class, "data");
        assertHasGenericField(MyArrayList.class, "data");
        assertClassImplementsInterface(MyArrayList.class, MyList.class);
    }

    @Test
    public void should_have_default_constructor() throws Exception {
        assertHasDefaultConstructor(MyArrayList.class);
    }
}
