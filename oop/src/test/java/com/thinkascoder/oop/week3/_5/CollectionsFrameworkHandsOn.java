package com.thinkascoder.oop.week3._5;

import com.thinkascoder.system.HandsOn;
import com.thinkascoder.system.runner.HandsOnRunner;

import java.util.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class CollectionsFrameworkHandsOn extends HandsOnRunner {
    @HandsOn("В стандартной библеотеке Collections Framework в JDK есть уже реализованный ArrayList, который тебе уже хорошо знаком")
    public void arrayList_example() {
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);

        assertEquals(ids.get(0), ___);
        assertEquals(ids.size(), ___);
        assertEquals(ids.indexOf(2), ___);
        assertEquals(ids.isEmpty(), ___);
        ids.clear();
        assertEquals(ids.isEmpty(), ___);
    }

    @HandsOn("Итератор - обьект с помощью которого можно пройти по коллекции значений")
    public void iterator_example() {
        // Например итератор который возвращает четные числа
        class EvenNumbersIterator implements Iterator<Integer> {
            // изначально итератор указывает до первого элемента
            int last = -2;

            @Override
            public boolean hasNext() {
                return true; // четные числа всегда есть
            }

            @Override
            public Integer next() {
                last = last + 2;
                return last;
            }
        }

        EvenNumbersIterator iterator = new EvenNumbersIterator();
        assertEquals(iterator.hasNext(), ___);
        assertEquals(iterator.next(), ___);

        assertEquals(iterator.hasNext(), ___);
        assertEquals(iterator.next(), ___);

        assertEquals(iterator.hasNext(), ___);
        assertEquals(iterator.next(), ___);

        assertEquals(iterator.hasNext(), ___);
        assertEquals(iterator.next(), ___);
    }

    @HandsOn("Напиши собственный итератор который будет итерировать по списку нечетных чисел")
    public void try_iterator() {
        class OddNumbersIterator implements Iterator<Integer> {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Integer next() {
                return 0;
            }
        }

        OddNumbersIterator iterator = new OddNumbersIterator();
        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), 1);

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), 3);

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), 5);

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), 7);
    }

    @HandsOn("Итерироваться по ArrayList очень удобно благодаря интерфейсу Iterable который возвращает Iterator")
    public void arrayList_iteration_example() {
        List<String> names = new ArrayList<>();
        names.add("Бобби");
        names.add("Джонни");
        names.add("Стивви");

        StringBuilder sb = new StringBuilder();

        for (String name : names) {
            sb.append(name);
        }
        assertEquals(sb.toString(), ___);

        // enhanced-for на самом деле итерируется с помощью итератора
        StringBuilder sb2 = new StringBuilder();

        Iterator<String> iterator = names.iterator();
        while (iterator.hasNext()) {
            sb2.append(iterator.next());
        }
        assertEquals(sb2.toString(), ___);
    }

    @HandsOn("Так же есть уже знакомный тебе LinkedList")
    public void linkedList_example() {
        List<String> names = new LinkedList<>();
        names.add("Бобби");
        names.add("Джонни");
        names.add("Стивви");

        assertEquals(names.get(1), ___);
        assertEquals(names.indexOf(0), ___);
        assertEquals(names.size(), ___);
        assertEquals(names.isEmpty(), ___);
        names.clear();
        assertEquals(names.isEmpty(), ___);
    }

    @HandsOn("По LinkedList тоже очень удобно с помощью for-each")
    public void linkedList_iteration_example() {
        List<String> names = new LinkedList<>();
        names.add("Бобби");
        names.add("Джонни");
        names.add("Стивви");

        StringBuilder sb = new StringBuilder();

        for (String name : names) {
            sb.append(name);
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn("LinkedList можно использовать как очередь, потому что LL имплементирует интерфейс Queue")
    public void linkedList_as_queue_example() {
        Queue<String> processesToPerform = new LinkedList<>();
        processesToPerform.add("Word");
        processesToPerform.add("Chrome");
        processesToPerform.add("Winamp");

        assertEquals(processesToPerform.size(), ___);
        assertEquals(processesToPerform.peek(), ___);
        assertEquals(processesToPerform.size(), ___);
        assertEquals(processesToPerform.poll(), ___);
        assertEquals(processesToPerform.size(), ___);
        processesToPerform.clear();
        assertEquals(processesToPerform.isEmpty(), ___);
    }

    @HandsOn("HashSet - математическое множество в котором нет дубликатов, " +
            "оно используется для определения принадлежит ли элемент множеству или нет")
    public void hashSet_example() {
        Set<String> colors = new HashSet<>();
        colors.add("красный");
        colors.add("черный");
        colors.add("синий");
        colors.add("черный");
        colors.add("красный");
        colors.add("красный");
        colors.add("красный");

        assertEquals(colors.size(), ___);
        assertEquals(colors.contains("красный"), ___);
        assertEquals(colors.contains("зеленый"), ___);
    }

    @HandsOn("for-each так-же дает возможность итерироваться по HashSet")
    public void hashSet_iteration_example() {
        Set<String> names = new HashSet<>();
        names.add("Игорь");
        names.add("Сережа");
        names.add("Игорь");
        names.add("Влад");

        StringBuilder sb = new StringBuilder();

        for (String name : names) {
            sb.append(name);
        }
        assertEquals(sb.toString(), ___);
    }

    @HandsOn("HashMap - отображение ключа на значение(соответстве). Простой пример - словарь..")
    public void hashmap_example() {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("road", "путь");
        dictionary.put("do", "делать");

        dictionary.put("study", "ошибочка вышла");
        dictionary.put("study", "учиться");

        assertEquals(dictionary.size(), ___);
        assertEquals(dictionary.get("do"), ___);
        assertEquals(dictionary.get("study"), ___);
        assertEquals(dictionary.containsKey("study"), ___);
        assertEquals(dictionary.containsValue("путь"), ___);
    }

    @HandsOn("Итерирование по HashMap можно делать с помощью enhanced for")
    public void hashmap_iteration_example() {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("road", "путь");
        dictionary.put("do", "делать");
        dictionary.put("study", "учиться");

        StringBuilder sb = new StringBuilder();
        Set<Map.Entry<String, String>> entries = dictionary.entrySet(); // множество сущностей в HashMap
        for (Map.Entry<String, String> entry : entries) {
            sb.append(entry.getKey());
            sb.append(" ");
            sb.append(entry.getValue());
            sb.append(", ");
        }

        assertEquals(sb.toString(), ___);
    }

    @HandsOn("С помощью класса Arrays.asList можно привести массив к коллекции List")
    public void array_to_list_example() {
        List<Integer> ints = Arrays.asList(4, 2, 3, 1);
        assertEquals(ints.size(), ___);

        assertEquals(ints, ___); // подсказка используй Arrays.asList
    }

    @HandsOn("Collections предоставляет набор полезных методов для работы с коллекциями")
    public void array_to_lists_example() {
        List<Integer> ints = Arrays.asList(4, 2, 3, 1);
        Collections.sort(ints);
        assertEquals(ints, ___);
        assertEquals(Collections.max(ints), ___);
        assertEquals(Collections.min(ints), ___);
    }
}
