package com.thinkascoder.oop.week3._3;

import com.thinkascoder.oop.week3._2.MyArrayList;
import com.thinkascoder.oop.week3._2.MyList;
import org.junit.Test;

import static com.thinkascoder.system.validations.ClassValidations.assertClassImplementsInterface;
import static com.thinkascoder.system.validations.ConstructorValidations.assertHasDefaultConstructor;
import static com.thinkascoder.system.validations.FieldValidations.assertFieldTypeWithGeneric;
import static com.thinkascoder.system.validations.FieldValidations.assertHasField;
import static com.thinkascoder.system.validations.FieldValidations.assertHasGenericField;

public class MyLinkedListStructureTest {
    @Test
    public void should_have_generic_array_field() throws Exception {
        assertHasField(MyLinkedList.class, "head", Node.class);
        assertFieldTypeWithGeneric(MyLinkedList.class, "head");
        assertClassImplementsInterface(MyLinkedList.class, MyList.class);
    }

    @Test
    public void should_have_default_constructor() throws Exception {
        assertHasDefaultConstructor(MyLinkedList.class);
    }
}
