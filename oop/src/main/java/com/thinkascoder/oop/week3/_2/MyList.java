package com.thinkascoder.oop.week3._2;

import com.thinkascoder.system.HomeworkNotCorrectException;

import java.util.Iterator;

public interface MyList<E> extends Iterable<E> {
    void add(E object);

    E get(int index);

    E remove(int index);

    boolean isEmpty();

    boolean contains(E object);

    void addAll(MyList<E> list);

    int size();

    void clear();

    int indexOf(E object);

    // Этот метод для задания 4, пока он тебе не нужен
    @Override
    default Iterator<E> iterator() {
        throw new HomeworkNotCorrectException("Переопредели метод iterator()");
    }
}
