package com.thinkascoder.oop.week3._4;

import java.util.Iterator;

/**
 * Итератор это паттерн, который используется для итерирования по коллекции обьектов.
 *
 * while(iterator.hasNext()) {
 *     System.out.println(iterator.next());
 * }
 *
 * Итератор - это указатель на определенный обьект в последовательности.
 * MyList<Integer> list = new MyArrayList<>();
 * list.add(1);
 * list.add(2);
 * list.add(3);
 *
 * // Пример с одним итератором:
 * Iterator<Integer> iterator = list.iterator();
 *
 * iterator.hasNext();    // вернет true
 * iterator.next();       // вернет 1
 * iterator.hasNext()     // вернет true
 * iterator.next();       // вернет 2
 * iterator.hasNext()     // вернет true
 * iterator.next();       // вернет 3
 * iterator.hasNext()     // вернет false
 * iterator.next();       // вернет null
 *
 * // Пример с двумя итераторами:
 *
 * Iterator<Integer> iterator1 = list.iterator();
 * Iterator<Integer> iterator2 = list.iterator();
 *
 * iterator1.next();       // вернет 1
 * iterator1.next();       // вернет 2
 * iterator1.next();       // вернет 3
 * iterator1.hasNext();    // вернет false
 *
 * iterator2.hasNext();    // вернет true
 * iterator2.next();       // вернет 1
 * iterator2.next();       // вернет 2
 * iterator2.next();       // вернет 3
 *
 *
 * Твоя задача:
 * - создать внутренний класс MyArrayListIterator, который имплементирует интрфейс java.util.Iterator в классе MyArrayList
 * - переопределить метод iterator() который будет возвращать новый итератор в классе MyArrayList
 * - создать внутренний класс MyLinkedListIterator, который имплементирует интрфейс java.util.Iterator в классе MyLinkedList
 * - переопределить метод iterator() который будет возвращать новый итератор в классе MyLinkedList
 *
 * (не забудь покрыть разные случаи тестами)
 */
public class IteratorTask {
    // Здесь код писать не надо
}
