package com.thinkascoder.oop.week4._7;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * На этот раз заказчик предоставил очень странное задание. Вместо подробного описания он дал пример работы программы.
 *
 * Welcome to FilesManager
 * > pwd
 * C:\
 * > cd folder
 * Changed directory to C:\folder
 * > pwd
 * C:\folder
 * > ls
 * images
 * book.txt
 * file2.txt
 * > rm book.txt
 * 1 file(s) deleted
 * > ls
 * images
 * file2.txt
 * > mkdir C:\folder\movies
 * 1 folder created
 * > ls
 * images
 * movies
 * file2.txt
 * > cd C:\folder\movies
 * Changed directory to C:\folder\movies
 * > pwd
 * C:\folder\movies
 * > touch new_file.txt
 * 1 file created
 * > ls
 * new_file.txt
 * > cd ..
 * Changed directory to C:\folder
 * > rm movies
 * Error: Can't remove folder (use -r to remove folder and all sub folders)
 * rm -r movies
 * > 1 folder and 1 file deleted
 * > exit
 * Good bye
 * // (программа закончила работу)
 *
 * Твоя задача:
 * 1. Понять и составить список функций программы, которые тебе потребуется реализовать(в виде списка на листике)
 * (постарайся понять, что каждая команда делает)
 * 2. Сделать консольное приложение FilesManager и реализовать все функции программы
 * 3. Воспроизвести последовательность действий как в примере и убедиться, что все работает именно так, как хочет заказчик
 *
 * Подсказки:
 * - не забывай, что в Windows папка по умолчанию C:\, а на linux системах /
 * (тебе придется узнать, на какой платформе работает твоя программа в данный момент)
 * - Обрати внимание, что эти команды очень напоминают команды работы с коммандной строкой в linux
 */
public class FilesManager {
    // Что бы использовать кодировку utf-8 по умолчанию
    static {
        System.setProperty("file.encoding", "UTF-8");
        Field charset = null;
        try {
            charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Scanner scanner = new Scanner(System.in);
    // Пиши свой код ниже
}
