package com.thinkascoder.oop.week2._2;

public class Dog {
    private String name;

    public String sayHello() {
        return "гав";
    }

    public String callOut() {
        return "иди кушать";
    }

    public String getName() {
        return name;
    }
}
