package com.thinkascoder.oop.week2._2;

public class Animal {
    private String name;

    public String sayHello() {
        return "звук";
    }

    public String callOut() {
        return "иди кушать";
    }

    public String getName() {
        return name;
    }
}
