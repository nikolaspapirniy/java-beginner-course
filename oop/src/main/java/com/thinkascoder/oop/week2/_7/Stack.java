package com.thinkascoder.oop.week2._7;

public interface Stack {
    void push(Object element);
    Object pop();
    Object peek();

    Object size();
}
