package com.thinkascoder.oop.week2._8;

public interface Tree {
    void add(int value);
    boolean contains(int value);
    int max();
    int min();
    boolean remove(int value);
}
