package com.thinkascoder.oop.week2._2;

public class Cat {
    private String name;

    public String sayHello() {
        return "мяу";
    }

    public String callOut() {
        return "иди кушать";
    }

    public String getName() {
        return name;
    }
}
