package com.thinkascoder.oop.week2._2;

/**
 * Что бы лучше понять наследование и вообще для чего оно нужно, выполни ряд задач и посмотри, насколько легче
 * становится жизнь с наследованием.
 *
 * 1. Переименуй поле name в petName(вручную) в классах Animal, Cat, Dog, Fox.
 * Когда сделаешь, запусти RenameFieldSignatureTest
 *
 * 2. Переименуй метод getName(вручную) в метод getPetName в классах Animal, Cat, Dog, Fox.
 * Когда сделаешь, запусти RenameGetterSignatureTest
 *
 * Удобно "да"? Куча дублирующегося кода, который нужно поддерживать, может есть способ сделать лучше?
 * НАСЛЕДОВАНИЕ!
 * Мы знаем что кот это животное и собака, и лисица тоже животные, значит они все наследуются от животного(Animal)
 *
 * 3. Унаследуй классы зверьков от Animal
 * Когда сделаешь, запусти InheritClassesFromAnimalSignatureTest
 *
 * Заметил, что методы callOut, getPetName и поле petName одинаковы во всех классах и повторяются?
 * Ты же помнишь что они и так наследуются? Давай их уберем
 *
 * 4. Убери методы callOut, getPetName и поле petName из наследуемых классов
 * Когда сделаешь, запусти RemoveMethodsAndFieldSignatureTest
 *
 * Посмотри на свой код еще раз, теперь дубликатов нет. Давай проверим, стало ли удобнее?
 *
 * 5. Переименуй поле petName в name и метод getPetName в getPet
 * Когда сделаешь, запусти RenameFieldAndMethodsAgainSignatureTest
 *
 */
public class InheritanceTask {
    // здесь код писать не надо
}
