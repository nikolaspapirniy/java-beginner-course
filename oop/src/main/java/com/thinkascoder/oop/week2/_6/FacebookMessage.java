package com.thinkascoder.oop.week2._6;

public class FacebookMessage extends Message {
    private int facebookUserId;

    public FacebookMessage(String message, int facebookUserId) {
        super(message);
        this.facebookUserId = facebookUserId;
    }

    public int getFacebookUserId() {
        return facebookUserId;
    }

    @Override
    public String getMessage() {
        return "facebook - [" + facebookUserId + "] " + super.getMessage();
    }
}
