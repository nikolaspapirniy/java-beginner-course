package com.thinkascoder.oop.week2._2;

public class Fox {
    private String name;

    public String sayHello() {
        return "what does the fox say?";
    }

    public String callOut() {
        return "иди кушать";
    }

    public String getName() {
        return name;
    }
}
