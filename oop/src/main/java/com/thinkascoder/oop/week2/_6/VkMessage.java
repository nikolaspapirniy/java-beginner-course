package com.thinkascoder.oop.week2._6;

public class VkMessage extends Message {
    private int vkUserId;

    public VkMessage(String message, int vkUserId) {
        super(message);
        this.vkUserId = vkUserId;
    }

    public int getVkUserId() {
        return vkUserId;
    }

    @Override
    public String getMessage() {
        return "vk - [" + vkUserId + "] " + super.getMessage();
    }
}