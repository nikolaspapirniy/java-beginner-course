#!/usr/bin/env bash

if [ -z ${1+x} ]; then
    echo "Set project type";
    exit
fi

if [ -z ${2+x} ]; then
    echo "Set week number";
    exit
fi

autotest_framework_name="autochecker"
cd .. # release
cd .. # project
current_directory=${PWD##*/}
release_directory="$current_directory""-$1-week-$2"

cd .. # top level


rm -rf "$release_directory"
cp -r "$current_directory" "$release_directory"

# cd release dir
cd "$release_directory"

cd $autotest_framework_name

mvn test clean

cd ..

# remove system files
rm -f *.iml
rm -rf .idea
rm -rf .git
#rm -f .gitignore
rm -f .DS_Store

echo ${PWD##*/}


# remove files except study type (like basic)
rm -rf *[^$1^$autotest_framework_name^pom.xml]*

# remove extra weeks
rm -rf $1/src/main/java/com/thinkascoder/$1/week[^$2]
rm -rf $1/src/test/java/com/thinkascoder/$1/week[^$2]

rm -rf release
cd ..

zip -r -X "$release_directory" "$release_directory"

rm -rf "$release_directory"